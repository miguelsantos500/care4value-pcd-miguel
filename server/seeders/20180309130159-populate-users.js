'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.
    */
      return require("bcrypt").hash('123qwerty', 10).then(function(passwordHash) {

        return queryInterface.bulkInsert('Users', [{
          name: 'admin',
          email: 'admin@mail.com',
          password: passwordHash,
          blocked: false,
          role: 'Administrator',
          token: 'null',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          name: 'researcher1',
          email: 'researcher1@mail.com',
          password: passwordHash,
          blocked: false,
          role: 'Researcher',
          token: 'null',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          name: 'researcher2',
          email: 'researcher2@mail.com',
          password: passwordHash,
          blocked: false,
          role: 'Researcher',
          token: 'null',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          name: 'other',
          email: 'other@mail.com',
          password: passwordHash,
          blocked: false,
          role: 'Health professional',
          token: 'null',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          name: 'blockedUserToTest',
          email: 'blockedUserToTest@mail.com',
          password: passwordHash,
          blocked: true,
          role: 'Health professional',
          token: 'null',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          name: 'unblockedUserToTest',
          email: 'unblockedUserToTest@mail.com',
          password: passwordHash,
          blocked: false,
          role: 'Health professional',
          token: 'null',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          name: 'unblockedUserToTest1',
          email: 'unblockedUserToTest1@mail.com',
          password: passwordHash,
          blocked: false,
          role: 'Health professional',
          token: 'null',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          name: 'blockButtonTestUser',
          email: 'blockButtonTestUser@mail.com',
          password: passwordHash,
          blocked: false,
          role: 'Researcher',
          token: 'null',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          name: 'unblockButtonTestUser',
          email: 'unblockButtonTestUser@mail.com',
          password: passwordHash,
          blocked: true,
          role: 'Researcher',
          token: 'null',
          createdAt: new Date(),
          updatedAt: new Date()
        },
            {
                name: 'userToChange',
                email: 'userToChange@mail.com',
                password: passwordHash,
                blocked: false,
                role: 'Health professional',
                token: 'null',
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                name: 'password',
                email: 'password@mail.com',
                password: passwordHash,
                blocked: false,
                role: 'Health professional',
                token: 'null',
                createdAt: new Date(),
                updatedAt: new Date()
            }], {});

      });
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.
    */
      return queryInterface.bulkDelete('Users', null, {});
    
  }
};
