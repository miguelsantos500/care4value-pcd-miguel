'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {

    const db = require('../sequelize')(process.env.TESTING);
    const Scale = require('../models/scale')(db.sequelize, db.Sequelize);
    const Question = require('../models/question')(db.sequelize, db.Sequelize);
    const ScaleQuestion = require('../models/scalequestion')(db.sequelize, db.Sequelize);
    Question.belongsToMany(Scale, {through: ScaleQuestion, foreignKey: 'questionId'});
    Scale.belongsToMany(Question, {through: ScaleQuestion, foreignKey: 'scaleId'});

    const scalesToInsert = [
      {
        name: "Barthel",
        questions: ["1-Beber por uma chávena", "2- Comer", "3- Vestir a parte superior do corpo",
          "4- Vestir a parte inferior do corpo", "6- Toalhete pessoal", "7- Lavar-se ou tomar banho",
          "8- Controlo da urina", "9- Controlo dos intestinos", "10- Sentar-se e levantar-se da cadeira",
          "11- Sentar-se e levantar-se da sanita", "12- Entrar e sair da banheira ou duche", "13- Andar 50 metros em piso plano",
          "14- Subir e descer um lanço de escadas", "15- SE NÃO ANDAR, impulsionar ou puxar uma cadeira de rodas",
          "Pontuação", "Interpretação do resultado", "Ações a realizar"]
      },
      {
        name: "Braden",
        questions: ["Percepção Sensorial (capacidade de reacção significativa ao desconforto)",
          "Humidade", "Actividade", "Mobilidade", "Nutrição", "Fricção e Forças de Deslizamento",
          "Pontuação", "Interpretação do resultado", "Ações a realizar"]
      },
      {
        name: "Gijon",
        questions: ["Turno", "Situação Familiar", "Relações e Contatos Sociais",
          "Apoio da Rede Social", "Pontuação Total"]
      },
      {
        name: "MNA",
        questions: ["A. Nos últimos três meses houve diminuição da ingestão alimentar devido a perda de apetite, problemas digestivos ou dificuldade para mastigar ou deglutir?",
          "B. Perda de peso nos últimos 3 meses", "C. Mobilidade",
          "D. Passou por algum stress psicológico ou doença aguda nos últimos três meses?",
          "E. Problemas Psocológicos/Neurológicos", "F1. Índice de Massa Corporal (IMC = peso [KG] / estatura [m]2)",
          "F2. Circunferência da perna (CP) em cm", "Idade", "Pontuação Total"]
      },
      {
        name: "MNS",
        questions: ["Turno", "Orientação - Em que ano estamos?", "Orientação - Em que mês estamos?",
          "Orientação - Em que dia do mês estamos?", "Orientação - Em que dia da semana estamos?",
          "Orientação - Em que estação do ano estamos?", "Orientação - Em que país estamos?",
          "Orientação - Em que distrito vive?", "Orientação - Em que terra vive?",
          "Orientação - Em que casa estamos?", "Orientação - Em que andar estamos?",
          "Retenção - Pêra", "Retenção - Gato", "Retenção - Bola", "Atenção e Cálculo - 27",
          "Atenção e Cálculo - 24", "Atenção e Cálculo - 21", "Atenção e Cálculo - 18", "Atenção e Cálculo - 15",
          "Evocação - Pêra", "Evocação - Gato", "Evocação - Bola", "Linguagem - Como se chama isto? Relógio",
          "Linguagem - Como se chama isto? Lápis", "Linguagem - O RATO ROEU A ROLHA", "Linguagem - Pega com a mão direita",
          "Linguagem - Dobra ao meio", "Linguagem - Coloca onde deve", "Linguagem - Fechou os olhos",
          "Linguagem - Escrever uma frase inteira", "Habilidade Construtiva - Copiar um desenho", "Pontuação Total"]
      }
    ];

    const promises = [];
    //Will Insert/Update Questions for each Scale
    scalesToInsert.forEach((scaleToInsert) => {
      let p = Scale.create({
        name: scaleToInsert.name
      }).then((scaleAux) => {

        return Scale.findOne({where: {id: scaleAux.id}})
          .then((scale) => {
            let questionsStringArray = scaleToInsert.questions;
            console.log(JSON.stringify(scale));

            let promiseArray = [];
            for (let i = 0; i < questionsStringArray.length; i++) {
              promiseArray.push(
                //Checks if there is already a question with that name
                Question.findOrCreate({
                  where: {name: questionsStringArray[i]},
                  defaults: {name: questionsStringArray[i]}
                })
              );
            }
            return Promise.all(promiseArray).then((questions) => {
              console.log(JSON.stringify(questions));;
              let promiseArray = [];
              //console.log(JSON.stringify(questions));
              for (let i = 0; i < questions.length; i++) {
                //Adds N-to-N
                let question = typeof questions[i].length !== undefined ?  questions[i][0] : questions[i];
                promiseArray.push(question.addScale(scale.id));
              }
              return Promise.all(promiseArray).then(() => 'ok');
            });
          });

      });
      promises.push(p);
    });

    return Promise.all(promises);


  },

  down: (queryInterface, Sequelize) => {

    return queryInterface.bulkDelete('ScaleQuestions', null, {})
      .then(() => {
        return queryInterface.bulkDelete('Questions', null, {})
      }).then(() => {
        return queryInterface.bulkDelete('Scales', null, {});
      });

  },
};


/*
  Add reverting commands here.
  Return a promise to correctly handle asynchronicity.

  Example:
  return queryInterface.bulkDelete('Person', null, {});
*/
