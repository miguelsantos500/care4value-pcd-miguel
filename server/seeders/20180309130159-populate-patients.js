'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.
    */
    return queryInterface.bulkInsert('Patients', [
      {
        name: 'Patient1',
        code: 'P0001',
        birthDate: new Date(),
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'Patient2',
        code: 'P0002',
        birthDate: new Date(),
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'Patient3',
        code: 'P0003',
        birthDate: new Date(),
        createdAt: new Date(),
        updatedAt: new Date()
      },
    ], {});


  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.
    */
    return queryInterface.bulkDelete('Users', null, {});

  }
};
