'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Configurations', [{
          platform_email: 'care4value.project@gmail.com',
          platform_email_properties: '',
          createdAt: new Date(),
          updatedAt: new Date()
        }], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Configurations', null, {});
  }
};
