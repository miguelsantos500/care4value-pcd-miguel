'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('ScaleQuestions', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      scaleId: {
        type: Sequelize.INTEGER,
        references: {
          model: 'Scales',
          key: 'id'
        },
        allowNull: false
      },
      questionId: {
        type: Sequelize.INTEGER,
        references: {
          model: 'Questions',
          key: 'id'
        },
        allowNull: false
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })/*.then(() => {
      return queryInterface.sequelize.query('ALTER TABLE ScaleQuestions ADD CONSTRAINT composite_key PRIMARY KEY (ScaleId, QuestionId)');
    })*/;
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('ScaleQuestions');
  }
};