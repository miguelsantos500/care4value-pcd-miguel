'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
        'Users',
        'blocked',
        {
          type: Sequelize.BOOLEAN
        }
      ).then(() => {
        return queryInterface.addColumn(
          'Users',
          'role',
          {
            type: Sequelize.ENUM('Administrator', 'Researcher', 'Health professional'),
            allowNull: false
          }
        ).then(() => {
          return queryInterface.removeColumn('Users', 'admin');
        });
      });
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.removeColumn('Users', 'blocked').then(() => {
        return queryInterface.removeColumn('Users', 'role').then(() => {
          return queryInterface.addColumn(
            'Users', 
            'admin',
            {
              type: Sequelize.BOOLEAN
            }
          );
        });
      });
  }
};
