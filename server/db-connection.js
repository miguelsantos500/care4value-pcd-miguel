const log = require('./support/logger');

module.exports = (testing) => {
//DataBase Implementation. Using Sequelize ORM
  log.debug('Setting the Database to Testing Mode? ' + !!testing);
  const db = require('./sequelize')(testing);

  db.sequelize
    .authenticate()
    .then(() => {
      log.debug('Connection to database has been established successfully.');
    })
    .catch(err => {
      log.error('Unable to connect to the database:', err);
    });

  const User = require('./models/user')(db.sequelize, db.Sequelize);

  const Patient = require('./models/patient')(db.sequelize, db.Sequelize);

  const Scale = require('./models/scale')(db.sequelize, db.Sequelize);
  const Question = require('./models/question')(db.sequelize, db.Sequelize);

  const ScaleQuestion = require('./models/scalequestion')(db.sequelize, db.Sequelize);
  Question.belongsToMany(Scale, {through: ScaleQuestion, foreignKey: 'questionId'});
  Scale.belongsToMany(Question, {through: ScaleQuestion, foreignKey: 'scaleId'});

  const Answer = require('./models/answer')(db.sequelize, db.Sequelize);
  Answer.belongsTo(ScaleQuestion, {foreignKey: 'scaleQuestionId'});
  Answer.belongsTo(Patient, {foreignKey: 'patientId'});
  Answer.belongsTo(Scale, {foreignKey: 'scaleId'});

  const PasswordReset = require('./models/passwordreset')(db.sequelize, db.Sequelize);
  return {
    User: User, Patient: Patient, Scale: Scale, Question: Question, ScaleQuestion: ScaleQuestion,
    Answer: Answer, PasswordReset: PasswordReset,
    db: db
  };
};