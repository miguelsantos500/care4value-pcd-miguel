const log = require('./support/logger');

module.exports = function (testing) {
    const Sequelize = require('sequelize');
    let dbName = 'care4value';
    if(testing) dbName = dbName+'_testing';

    const sequelize = new Sequelize(dbName, 'root', 'finalProject2018', {
        host: 'localhost',
        dialect: 'mysql' /*|'sqlite'|'postgres'|'mssql'*/,
        operatorsAliases: false,
        // logging:false,
        pool: {
            max: 5,
            min: 0,
            acquire: 30000,
            idle: 10000
        },

    });

    return {Sequelize: Sequelize, sequelize: sequelize};
};
