/**
 * @license
 * Copyright Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
// [START gmail_quickstart]
const fs = require('fs');
const readline = require('readline');
const {google} = require('googleapis');

// If modifying these scopes, delete credentials.json.
const SCOPES = ['https://www.googleapis.com/auth/gmail.readonly'];
const TOKEN_PATH = require('path').resolve(__dirname, 'token.json');
let googleAuth = null;

const methods = {
  getFirstEmail: () => {
    return new Promise((resolve) => {

      console.log(googleAuth)
      const gmail = google.gmail({
        version: 'v1',
        googleAuth
      });

      gmail.users.messages.list({
        userId: 'me',
        maxResults: 1,
        // headers: {Authorization: 'Bearer ' + require(TOKEN_PATH).access_token},
        auth: googleAuth,
        q: 'in:sent'
      }, (err, response) => {
        if(err) console.log(err);
        let id = response.data.messages[0].id;
        // console.log(id);
        gmail.users.messages.get({
          userId: 'me',
          id: id,
          format: 'full',
          // headers: {Authorization: 'Bearer ' + require(TOKEN_PATH).access_token}
          auth: googleAuth,
        }, (err, response) => {
          if(err)  console.log(err);
          // console.log(JSON.stringify(response.data))
          let sentTo = response.data.payload.headers.find((o) => o.name === "To").value;
          let snippet = response.data.snippet;
          console.log("sentTo", sentTo);
          console.log("snippet", snippet);

          resolve({snippet: snippet, sentTo: sentTo});
        });


      });

    });

  }
};


module.exports = new Promise((resolve) => {
// Load client secrets from a local file.
  fs.readFile(require('path').resolve(__dirname, 'credentials.json'), (err, content) => {
    if (err) return console.log('Error loading client secret file:', err);
    // Authorize a client with credentials, then call the Gmail API.
    authorize(JSON.parse(content.toString())).then((client) => {
      googleAuth = client;
      resolve(methods);
    });
  });
});


/**
 * Create an OAuth2 client with the given credentials, and then execute the
 * given callback function.
 * @param {Object} credentials The authorization client credentials.
 */
function authorize(credentials) {
  const {client_secret, client_id, redirect_uris} = credentials.installed;
  const oAuth2Client = new google.auth.OAuth2(
    client_id, client_secret, redirect_uris[0]);
  // Check if we have previously stored a token.
  return new Promise((resolve) => {
    fs.readFile(TOKEN_PATH, (err, token) => {
      if (err) getNewToken(oAuth2Client, resolve);
      oAuth2Client.setCredentials(JSON.parse(token));
      /*
      oAuth2Client.refreshAccessToken().then((tokens) => {
        console.log('New credentials: ', tokens.credentials);
        tokens.expiry_date = true;
        oAuth2Client.setCredentials(JSON.parse(JSON.stringify(tokens.credentials)));
        resolve(oAuth2Client);
      });*/
      resolve(oAuth2Client)
    });
  });
}

/**
 * Get and store new token after prompting for user authorization, and then
 * execute the given callback with the authorized OAuth2 client.
 * @param {google.auth.OAuth2} oAuth2Client The OAuth2 client to get token for.
 * @param {getEventsCallback} callback The callback for the authorized client.
 */
function getNewToken(oAuth2Client, callback) {
  const authUrl = oAuth2Client.generateAuthUrl({
    access_type: 'offline',
    scope: SCOPES,
  });
  console.log('Authorize this app by visiting this url:', authUrl);
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  });
  rl.question('Enter the code from that page here: ', (code) => {
    rl.close();
    oAuth2Client.getToken(code, (err, token) => {
      if (err) return callback(err);
      oAuth2Client.setCredentials(token);
      // Store the token to disk for later program executions
      fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err) => {
        if (err) return console.error(err);
        console.log('Token stored to', TOKEN_PATH);
      });
      callback(oAuth2Client);
    });
  });
}

/**
 * Lists the labels in the user's account.
 *
 * @param {google.auth.OAuth2} auth An authorized OAuth2 client.
 */
function listLabels(auth) {
  const gmail = google.gmail({version: 'v1', auth});
  gmail.users.labels.list({
    userId: 'me',
  }, (err, res) => {
    if (err) return console.log('The API returned an error: ' + err);
    const labels = res.data.labels;
    if (labels.length) {
      console.log('Labels:');
      labels.forEach((label) => {
        console.log(`- ${label.name}`);
      });
    } else {
      console.log('No labels found.');
    }
  });
}

// [END gmail_quickstart]