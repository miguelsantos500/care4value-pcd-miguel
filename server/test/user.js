//Require the dev-dependencies
let chai = global.chai ? global.chai : require('chai');
let chaiHttp = require('chai-http');
let {server} = require('../index');
let should = global.should ? global.should : chai.should();
let Model = global.Model ? global.Model : require('../db-connection')(true);

chai.use(chaiHttp);
global.chai = chai;
global.Model = Model;
global.should = should;


describe('User Testing', () => {
  before(function () {
    chai.request(server)
      .post('/testing')
      .send({password: 'finalProject2018', testing: true})
      .end((err, res) => {
        res.should.have.status(200);
      });
  });

  beforeEach((done) => { //Before each test
    //do something, then:
    done();

  });
  /*
  * Test the /login route
  */
  describe('/login testing', () => {
    it('it should login sucessfully', (done) => {
      chai.request(server)
        .post('/login')
        .send({email: 'admin@mail.com', password: '123qwerty'})
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.an('object');
          res.body.name.should.be.eql('admin');
          res.body.email.should.be.eql('admin@mail.com');
          res.body.role.should.be.eql('Administrator');
          res.body.token.should.exist;
          done();
        });
    });
    it('it should return unauthorized (no input)', (done) => {
      chai.request(server)
        .post('/login')
        .send()
        .end((err, res) => {
          res.should.have.status(401);
          res.body.should.be.an('object');
          res.body.message.should.match(/Missing credentials/);

          done();
        });
    });
    it('it should return unauthorized (invalid email)', (done) => {
      chai.request(server)
        .post('/login')
        .send({email: 'wrong@mail.com', password: 'wrong'})
        .end((err, res) => {
          res.should.have.status(401);
          res.body.should.be.an('object');
          res.body.message.should.match(/Incorrect credentials/);
          done();
        });
    });
    it('it should return unauthorized (invalid password)', (done) => {
      chai.request(server)
        .post('/login')
        .send({email: 'admin@mail.com', password: 'wrong'})
        .end((err, res) => {
          res.should.have.status(401);
          res.body.should.be.an('object');
          res.body.message.should.match(/Incorrect credentials/);
          done();
        });
    });

  });

  /*
  * Test the /logout route
  */
  describe('/logout testing', () => {
    it('it should logout sucessfully', (done) => {
      //Logs in first
      chai.request(server)
        .post('/login')
        .send({email: 'admin@mail.com', password: '123qwerty'})
        .end((err, res) => {
          res.should.have.status(200);
          let token = res.body.token;

          chai.request(server)
            .post('/logout')
            .send({token: token})
            .end((err, res) => {
              res.should.have.status(200);
              res.body.message.should.be.eql('OK!');
              done();
            });
        });
    });
    it('it should return bad request (no token)', (done) => {
      chai.request(server)
        .post('/logout')
        .end((err, res) => {
          res.should.have.status(400);
          res.body.message.should.match(/No Token Received!/);
          done();
        });
    });
    it('it should return user not found', (done) => {
      chai.request(server)
        .post('/logout')
        .send({token: 'A Wrong Token'})
        .end((err, res) => {
          res.should.have.status(404);
          res.body.message.should.match(/User not Found!/);
          done();
        });
    });
  });
  /*
  * Test the /users route
  */
  describe('/users testing', () => {
    it('it should return all users (if users fetched)', (done) => {
      //Logs in first
      chai.request(server)
        .post('/login')
        .send({email: 'admin@mail.com', password: '123qwerty'})
        .end((err, res) => {
          res.should.have.status(200);
          let token = res.body.token;

          chai.request(server)
            .get('/users')
            .set('Authorization', 'Bearer ' + token) //sets authorization header with logged in token
            .end((err, res) => {
              res.should.have.status(200);
              res.body.should.be.an('array'); //object received must be an array

              //get all users from db
              Model.User.findAll({
                attributes: {exclude: ['password', 'token']}
              }).then((testUsers) => {
                console.log('TEST - Fetched users:');
                console.log(JSON.stringify(testUsers));

                res.body.should.have.lengthOf(testUsers.length);

                //compares if the user on the response body equals the testUser
                for (let i = 0; i < testUsers.length; i++) {
                  let testUser = testUsers[i];
                  let user = res.body[i];

                  user.id.should.be.eql(testUser.id);
                  user.name.should.be.eql(testUser.name);
                  user.email.should.be.eql(testUser.email);
                  user.role.should.be.eql(testUser.role);
                  user.blocked.should.be.eql(testUser.blocked);
                  should.not.exist(user.password);
                  should.not.exist(user.token);
                }
                done();
              });
            });
        });
    });
    it('it should return "unauthorized" (no token)', (done) => {


      chai.request(server)
        .get('/users')
        .end((err, res) => {
          res.should.have.status(401);
          res.body.should.be.an('object'); //object received must be an object
          res.body.message.should.match(/No Token Received!/);

          done();
        });
    });
    it('it should return "unauthorized" (user does not exist)', (done) => {


      chai.request(server)
        .get('/users')
        .set('Authorization', 'Bearer testWrongToken')
        .end((err, res) => {
          res.should.have.status(401);
          res.body.should.be.an('object'); //object received must be an object
          res.body.message.should.match(/Incorrect credentials/);

          done();
        });
    });

    it('it should return "unauthorized" (user has no permissions to see this)', (done) => {
      //Logs in first
      chai.request(server)
        .post('/login')
        .send({email: 'other@mail.com', password: '123qwerty'})
        .end((err, res) => {
          res.should.have.status(200);
          let token = res.body.token;

          chai.request(server)
            .get('/users')
            .set('Authorization', 'Bearer ' + token) //sets authorization header with logged in token
            .end((err, res) => {
              res.should.have.status(401);
              res.body.should.be.an('object'); //object received must be an array

              res.body.message.should.match(/User is not Admin!/);

              done();
            });
        });
    }); //ended it should return "unauthorized" (user has no permissions to see this)

    it('it should add user successfully', (done) => {
      //Logs in first
      chai.request(server)
        .post('/login')
        .send({email: 'admin@mail.com', password: '123qwerty'})
        .end((err, res) => {
          res.should.have.status(200);
          let token = res.body.token;


          let userToAdd = {
            name: 'createUserTestName',
            email: 'createUserTestEmail@mail.com',
            password: 'test',
            role: 'Researcher'
          };

          //Deletes user if already exists
          Model.User.destroy({
            where: {
              email: userToAdd.email
            }
          }).then(() => {
            chai.request(server)
              .post('/users/add')
              .set('Authorization', 'Bearer ' + token)
              .send({user: userToAdd})
              .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.an('object');

                Model.User.findOne({where: {email: userToAdd.email}}).then(user => {
                  user.should.be.an('object');
                  user.name.should.be.eql(userToAdd.name);
                  user.email.should.be.eql(userToAdd.email);
                  user.role.should.be.eql(userToAdd.role);

                  done();
                });

              });

          });
        });
    }); //End of it should add user successfully

    it('it shloudn\'t add user if required parameters are missing', (done) => {
      chai.request(server)
        .post('/login')
        .send({email: 'admin@mail.com', password: '123qwerty'})
        .end((err, res) => {
          res.should.have.status(200);
          let token = res.body.token;

          //create a user to add without role field
          let userToAdd = {
            name: 'createUserTestName',
            email: 'createUserTestEmail@mail.com',
            password: 'test'
          };

          //Deletes user if already exists
          Model.User.destroy({
            where: {
              email: userToAdd.email
            }
          }).then(() => {
            //Tries to create user missing required fields
            chai.request(server)
              .post('/users/add')
              .set('Authorization', 'Bearer ' + token)
              .send({user: userToAdd})
              .end((err, res) => {
                res.should.have.status(400);
                res.body.message.should.be.eql('Missing Parameter: role');
                res.body.should.be.an('object');

                done();
              });

          });
        });
    });//End of it shloudn't add user if required parameters are missing


    it('it shouldn\'t add user if current user doesn\'t have permission', (done) => {
      chai.request(server)
        .post('/login')
        .send({email: 'researcher1@mail.com', password: '123qwerty'})
        .end((err, res) => {
          res.should.have.status(200);
          let token = res.body.token;

          //create a user to add without role field
          const userToAdd = {
            name: 'createUserTestName',
            email: 'createUserTestEmail@mail.com',
            password: 'test',
            role: 'Researcher'
          };

          //Deletes user if already exists
          Model.User.destroy({
            where: {
              email: userToAdd.email
            }
          }).then(() => {
            //Tries to create user missing required fields
            chai.request(server)
              .post('/users/add')
              .set('Authorization', 'Bearer ' + token)
              .send({user: userToAdd})
              .end((err, res) => {
                res.should.have.status(401);
                res.body.message.should.match(/User is not Admin!/);
                res.body.should.be.an('object');

                done();
              });

          });
        });
    });//End of 'it shouldn't add user if current user doesn't have permission'

  }); //Ended describe
  /*
  * Test the /users/change-password route
  */
  describe('/change-password testing', () => {
    it('it should change password successfully', (done) => {

      //change password to oldPassword via Database
      require("bcrypt").hash('oldPassword', 10).then(function (passwordHash) {
        Model.User.update({password: passwordHash},
          {where: {email: 'researcher1@mail.com'}}).then((count) => {
          //login to get token
          chai.request(server)
            .post('/login')
            .send({email: 'researcher1@mail.com', password: 'oldPassword'})
            .end((err, res) => {
              res.should.have.status(200);
              let token = res.body.token;
              //change password to 123qwerty via API
              chai.request(server)
                .put('/users/self/change-password')
                .set('Authorization', 'Bearer ' + token) //sets authorization header with logged in token
                .send({newPassword: '123qwerty'})
                .end((err, res) => {
                  res.should.have.status(200);
                  res.body.should.be.an('object');
                  res.body.message.should.be.eql('Password Changed');

                  //Validate if can do Login
                  chai.request(server)
                    .post('/login')
                    .send({email: 'researcher1@mail.com', password: '123qwerty'})
                    .end((err, res) => {
                      res.should.have.status(200);
                      done();

                    });
                });
            });
        });
      });
    });
    it('it should return unauthorized (Wrong Token)', (done) => {
      chai.request(server)
        .put('/users/self/change-password')
        .set('Authorization', 'Bearer wrongToken')
        .send({newPassword: '123qwerty'})
        .end((err, res) => {
          res.should.have.status(401);
          res.body.should.be.an('object');
          res.body.message.should.match(/Incorrect credentials/);
          done();
        });
    });


    it('it should revoke permissions to user (block user)', (done) => {
      chai.request(server)
        .post('/login')
        .send({email: 'admin@mail.com', password: '123qwerty'})
        .end((err, res) => {
          res.should.have.status(200);
          let token = res.body.token;

          Model.User.findOne({where: {email: 'unblockedUserToTest@mail.com'}}).then(user => {
            //unblocks user before testing (to be shure the user is unblocked)
            Model.User.update({blocked: false},
              {where: {email: 'unblockedUserToTest@mail.com'}}).then(count => {

              chai.request(server)
                .put('/users/block')
                .set('Authorization', 'Bearer ' + token)
                .send({userId: user.id})
                .end((err, res) => {
                  res.should.have.status(200);
                  res.body.should.be.an('object');

                  Model.User.findOne({where: {email: 'unblockedUserToTest@mail.com'}}).then(user => {
                    user.should.be.an('object');
                    user.blocked.should.be.eql(true);

                    done();
                  });
                });
            });
          });
        });
    });

    it('it should grant presmissions to user (unblock user)', (done) => {
      chai.request(server)
        .post('/login')
        .send({email: 'admin@mail.com', password: '123qwerty'})
        .end((err, res) => {
          res.should.have.status(200);
          let token = res.body.token;

          Model.User.findOne({where: {email: 'blockedUserToTest@mail.com'}}).then(user => {
            //blocks user before testing (to be shure the user is blocked)
            Model.User.update({blocked: true},
              {where: {email: 'blockedUserToTest@mail.com'}}).then(count => {

              chai.request(server)
                .put('/users/unblock')
                .set('Authorization', 'Bearer ' + token)
                .send({userId: user.id})
                .end((err, res) => {
                  res.should.have.status(200);
                  res.body.should.be.an('object');

                  Model.User.findOne({where: {email: 'blockedUserToTest@mail.com'}}).then(user => {
                    user.should.be.an('object');
                    user.blocked.should.be.eql(false);

                    done();
                  });
                });
            });
          });

        });
    });

    it('it should return "unauthorized" (user has no permissions to do this)', (done) => {
      //Logs in first
      chai.request(server)
        .post('/login')
        .send({email: 'other@mail.com', password: '123qwerty'})
        .end((err, res) => {
          res.should.have.status(200);
          let token = res.body.token;

          Model.User.findOne({where: {email: 'blockedUserToTest@mail.com'}}).then(user => {
            //blocks user before testing (to be shure the user is blocked)
            Model.User.update({blocked: true},
              {where: {email: 'unblockedUserToTest1@mail.com'}}).then(count => {
              chai.request(server)
                .put('/users/block')
                .set('Authorization', 'Bearer ' + token)
                .send({userId: user.id})
                .end((err, res) => {
                  res.should.have.status(401);
                  res.body.should.be.an('object'); //object received must be an array

                  res.body.message.should.match(/User is not Admin!/);

                  done();
                });
            });
          });
        });
      // });

      // });
    });

  }); //End of describe
  /*
* Test the /users/reset-password route
 */
  describe('/reset-password testing', () => {
    it('it should send a confirmation email', (done) => {

      require('./gmail/index').then((gmail) => {
        chai.request(server)
          .post('/password/reset')
          .send({email: 'admin@mail.com'})
          .end((err, res) => {
            gmail.getFirstEmail().then((content) =>{
              content.sentTo.should.be.eql('admin@mail.com');
              content.snippet.should.be.eql('Please, click the link to reset your password. password reset link');
              done();

            });

          });
      });


    });
  }); //End of describe
  /*
  * Test the /users/:id route
  */
  describe('update user testing', () => {
    it('it should change user successfully', (done) => {


      //Prepare Test: Find User and set to normal
      Model.User.findOne({where: {email: 'userToChange@mail.com'}}).then(user => {
        Model.User.update({name: 'oldName', role: 'Administrator'},
          {where: {email: 'userToChange@mail.com'}}).then((count) => {


          chai.request(server)
            .post('/login')
            .send({email: 'admin@mail.com', password: '123qwerty'})
            .end((err, res) => {
              res.should.have.status(200);
              let token = res.body.token;
              //change user via API
              let updatedUser = {email: 'userToChange@mail.com', name: 'newName', role: 'Researcher'};
              chai.request(server)
                .put('/users/update/' + user.id)
                .set('Authorization', 'Bearer ' + token)
                .send({user: updatedUser})
                .end((err, res) => {
                  res.should.have.status(200);
                  res.body.should.be.an('object');
                  res.body.message.should.be.eql('User Updated');


                  Model.User.findOne({where: {email: 'userToChange@mail.com'}}).then(newUser => {
                    newUser.name.should.be.eql(updatedUser.name);
                    newUser.email.should.be.eql(updatedUser.email);
                    newUser.role.should.be.eql(updatedUser.role);

                    done();
                  });
                });
            });


        });
      });
      it('it shouldn\'t change user if current user doesn\'t have permission', (done) => {

        Model.User.findOne({where: {email: 'userToChange@mail.com'}}).then(user => {
          Model.User.update({name: 'oldName', role: 'Administrator'},
            {where: {email: 'userToChange@mail.com'}}).then((count) => {


            chai.request(server)
              .post('/login')
              .send({email: 'other@mail.com', password: '123qwerty'})
              .end((err, res) => {
                res.should.have.status(200);
                let token = res.body.token;
                //change user via API
                let updatedUser = {email: 'userToChange@mail.com', name: 'newName', role: 'Researcher'};
                chai.request(server)
                  .put('/users/update/' + user.id)
                  .set('Authorization', 'Bearer ' + token)
                  .send({user: updatedUser})
                  .end((err, res) => {
                    res.should.have.status(401);
                    res.body.should.be.an('object');
                    res.body.message.should.match(/User is not Admin!/);

                    /*
                                      Model.User.findOne({ where: {email: 'userToChange@mail.com'} }).then(newUser => {
                                        newUser.name.should.be.eql(updatedUser.name);
                                        newUser.email.should.be.eql(updatedUser.email);
                                        newUser.role.should.be.eql(updatedUser.role);*/

                    done();
                  });
              });
          });
        });
      });
    });


  });
}); //end of file

