//Require the dev-dependencies
let chai = global.chai ? global.chai : require('chai');
let chaiHttp = require('chai-http');
let {server} = require('../index');
let should = global.should ? global.should : chai.should();
let Model = global.Model ? global.Model : require('../db-connection')(true);

chai.use(chaiHttp);
global.chai = chai;
global.Model = Model;
global.should = should;


describe('Scale Testing', () => {
  before(function () {
    chai.request(server)
      .post('/testing')
      .send({password: 'finalProject2018', testing: true})
      .end((err, res) => {
        res.should.have.status(200);
      });
  });

  beforeEach((done) => { //Before each test
    //do something, then:
    done();

  });
  /*
  * Test the /json route
  */
  describe('/json', () => {
    it('it should store', (done) => {
      chai.request(server)
        .post('/login')
        .send({email: 'admin@mail.com', password: '123qwerty'})
        .end((err, res) => {
          res.should.have.status(200);
          let token = res.body.token;
          let date = '2018-01-01T07:46:23.000Z';
          let patientName = 'Server Testing';
          const content = {
            "questions": [{"question": "1-Beber por uma chávena", "answer": "Pode por si próprio\n", "groupTotal": "4"},
              {"question": "2- Comer", "answer": "Pode por si próprio\n", "groupTotal": "6"},
              {"question": "3- Vestir a parte superior do corpo", "answer": "Pode por si próprio\n", "groupTotal": "5"},
              {"question": "4- Vestir a parte inferior do corpo", "answer": "Pode com ajuda\n", "groupTotal": "4"},
              {"question": "6- Toalhete pessoal", "answer": "Pode por si próprio\n", "groupTotal": "5"},
              {"question": "7- Lavar-se ou tomar banho", "answer": "Pode com ajuda\n", "groupTotal": "0"},
              {"question": "8- Controlo da urina", "answer": "Pode por si próprio\n", "groupTotal": "10"},
              {"question": "9- Controlo dos intestinos", "answer": "Pode por si próprio\n", "groupTotal": "10"},
              {"question": "10- Sentar-se e levantar-se da cadeira", "answer": "Pode com ajuda\n", "groupTotal": "7"},
              {"question": "11- Sentar-se e levantar-se da sanita", "answer": "Pode com ajuda\n", "groupTotal": "3"},
              {"question": "12- Entrar e sair da banheira ou duche", "answer": "Pode com ajuda\n", "groupTotal": "0"},
              {"question": "13- Andar 50 metros em piso plano", "answer": "Não pode\n", "groupTotal": "0"},
              {"question": "14- Subir e descer um lanço de escadas", "answer": "Não pode\n", "groupTotal": "0"},
              {
                "question": "15- SE NÃO ANDAR, impulsionar ou puxar uma cadeira de rodas",
                "answer": "Pode por si próprio\n",
                "groupTotal": "5"
              },
              {"question": "Pontuação", "answer": 59}, {
                "question": "Interpretação do resultado",
                "answer": "Grau de Dependência Moderado"
              },
              {"question": "Ações a realizar", "answer": "Sem ações definidas"}],
            "author": "Server Testing",
            "date": date
          };
          chai.request(server)
            .post('/store-content/json')
            .send({patientId: 1, contentType: 'Barthel', content: content})
            .set('Authorization', 'Bearer ' + token)
            .end((err, res) => {
              res.should.have.status(200);
              Model.Answer.findAll({where: {date: date, patientId: 1}}).then((fetchedAnswers) => {
                fetchedAnswers.should.have.lengthOf(17);
                done();
              });
            });
        });
    });
  });
});