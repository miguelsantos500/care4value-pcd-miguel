const log = require('../support/logger');

const express = require('express');
const app = express();

module.exports = (passport, Model, genericFunctions) => {

  //Returns all users on bd
  app.get('/', passport.authenticate('custom-bearer', {failWithError: true}),
    passport.authenticate('admin', {failWithError: true}),
    function (req, res, next) {
      log.debug('Entered /users API. Fetching users...');
      Model.User.findAll({
        attributes: {exclude: ['password', 'token']}
      }).then((users) => {
        log.debug('Fetched users:');
        log.debug(JSON.stringify(users));


        res.send(users);
      });
    },
    genericFunctions.error);

  //Creates a new user on users table
  app.post('/add', passport.authenticate('custom-bearer', {failWithError: true}),
    passport.authenticate('admin', {failWithError: true}),
    function (req, res, next) {
      log.debug('Entered /add API. Adding new user to DB...');

      log.debug('User received:');
      log.debug(req.body.user);

      //Stores the required fields for validation and error message
      const requiredFields = ['name', 'email', 'password', 'role'];

      for (let i = 0; i < requiredFields.length; i++) {
        if (!req.body.user[requiredFields[i]]) {
          let message = 'Missing Parameter: ' + requiredFields[i];
          log.debug(message);
          return res.status(400).send({message: message});
        }
      }

      return require("bcrypt").hash(req.body.user.password, 10).then(function (passwordHash) {
        Model.User.create({
          name: req.body.user.name,
          email: req.body.user.email,
          password: passwordHash,
          token: null,
          blocked: false,
          role: req.body.user.role
        }).then((user) => {
          log.debug('Added user:');
          log.debug(JSON.stringify(user));
          res.send(user);
        }).catch((exception) => {
          res.status(400).send(genericFunctions.getSequelizeErrors(exception));
        });
      }); //End of User.create()

    },
    genericFunctions.error);

  //Updates User - Admin Only
  app.put('/update/:id', passport.authenticate('custom-bearer', {failWithError: true}),
    passport.authenticate('admin', {failWithError: true}),
    function (req, res, next) {
      log.debug('Entered update user API.');
      if (!req.body) {
        let message = 'No User Given!';
        log.debug(message);
        return res.status(400).send({message: message});
      }
      //Stores the required fields for validation and error message
      const requiredFields = ['name', 'email', 'role'];

      let user = req.body.user;
      for (let i = 0; i < requiredFields.length; i++) {
        if (!user[requiredFields[i]]) {
          let message = 'Missing Parameter: ' + requiredFields[i];
          log.debug(message);
          return res.status(400).send({message: message});
        }
      }

      Model.User.update({
          name: user.name,
          email: user.email,
          role: user.role
        },
        {where: {id: req.params.id}}).then((count) => {
        if (count) {
          res.send({message: 'User Updated'});
        } else {
          res.status(400).send({message: 'No User Found!'});
        }
      });


    },
    genericFunctions.error);

  //Changes User Password
  app.put('/self/change-password', passport.authenticate('custom-bearer', {failWithError: true}),
    function (req, res, next) {
      if (!req.body.newPassword) {
        let message = 'No Password Given!';
        log.debug(message);
        return res.status(400).send({message: message});
      }
      log.debug('Entered /change-password API.');

      require("bcrypt").hash(req.body.newPassword, 10).then(function (passwordHash) {
        Model.User.findOne({where: {token: req.token}}).then((user) => {
          if (!user) {
            let message = 'No User Found!';
            log.debug(message);
            return res.status(400).send({message: message});
          }
          log.debug('Updating User ' + user.id + 'with hash of password ' + req.body.newPassword);
          Model.User.update({password: passwordHash},
            {where: {id: user.id}}).then((count) => {
            res.send({message: 'Password Changed'});
          });
        });
      });
    },
    genericFunctions.error);

//Blocks a user on users table
  app.put('/block', passport.authenticate('custom-bearer', {failWithError: true}),
    passport.authenticate('admin', {failWithError: true}),
    function (req, res, next) {
      if (!req.body.userId) {
        let message = 'No User Id Given!';
        log.debug(message);
        return res.status(400).send({message: message});
      }
      log.debug('Entered /block API. Blocking user...');

      log.debug('User Id received:');
      log.debug(req.body.userId);

      Model.User.update({blocked: true},
        {where: {id: req.body.userId}}).then((count) => {
        res.send({message: 'User updated: user blocked.'});
      });
    },
    genericFunctions.error);

//Unlocks a user on users table
  app.put('/unblock', passport.authenticate('custom-bearer', {failWithError: true}),
    passport.authenticate('admin', {failWithError: true}),
    function (req, res, next) {
      if (!req.body.userId) {
        let message = 'No User Id Given!';
        log.debug(message);
        return res.status(400).send({message: message});
      }
      log.debug('Entered /unblock API. Blocking user...');

      log.debug('User Id received:');
      log.debug(req.body.userId);

      Model.User.update({blocked: false},
        {where: {id: req.body.userId}}).then((count) => {
        res.send({message: 'User updated: user unblocked.'});
      });
    },
    genericFunctions.error);

  return app;
};
