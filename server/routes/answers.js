const log = require('../support/logger');
const express = require('express');
const app = express();

module.exports = (passport, Model, genericFunctions) => {

  //Returns answers on bd
  app.get('/', passport.authenticate('custom-bearer', {failWithError: true}),
    (req, res, next) => {
      log.debug('Entered /answers API. Fetching answers...');

      const searchQuery = {
        attributes: {exclude: ['password', 'token']}
      };
      if (req.query.date) {
        searchQuery.where = {date: req.query.date};
      }
      if (req.query.patient) {
        if (!searchQuery.where) {
          searchQuery.where = {};
        }
        searchQuery.where.patient = req.query.patient;
      }
      searchQuery.include = [{
        model: Model.Scale,
      }, {
        model: Model.Patient
      }];
      log.debug('Answers Query:', JSON.stringify(searchQuery));
      Model.Answer.findAll(searchQuery).then((answers) => {
        log.debug('Fetched answers: ', answers.length);
        // log.debug(JSON.stringify(answers));

        //changes key 'Patient' to 'patient'
        let replaceKeyInObjectArray = (a, r) => a.map(o =>
          Object.keys(o).map((key) => ({[r[key] || key]: o[key]})
          ).reduce((a, b) => Object.assign({}, a, b)));
        answers = replaceKeyInObjectArray(JSON.parse(JSON.stringify(answers)), {
          'Patient': 'patient',
          'Scale': 'scale'
        });
        res.send(answers);
      });
    },
    genericFunctions.error);

  //returns first answers
  app.get('/first', passport.authenticate('custom-bearer', {failWithError: true}),
    (req, res, next) => {
      log.debug('Entered /answers/first API. Fetching answers...');

      Model.Answer.findAll({
        attributes: ['patientId', 'question', 'answer', 'scaleId', [Model.db.sequelize.fn('min', Model.db.sequelize.col('date')), 'minValue']],
        include: [{
          model: Model.Scale,
        }, {
          model: Model.Patient,
        },],
        group: ['question', 'patientId', 'scaleId']
      })
        .then((answers) => {
          log.debug('Entered /answers/first API. Fetching answers...');

          log.debug(answers.length);

          let answersArray = [];
          let patientAux = [];
          let scalesAux = [];

          for (let i = 0; i < answers.length; i++) {

            let pId = answers[i].patientId;
            let scaleId = answers[i].scaleId;

            patientAux.push(pId);
            scalesAux.push(scaleId);
          }

          patientAux.sort((a, b) => a - b);
          scalesAux.sort((a, b) => a - b);

          const onlyUnique = (value, index, self) => {
            return self.indexOf(value) === index;
          };

          const uniquePatients = patientAux.filter(onlyUnique);
          const uniqueScales = scalesAux.filter(onlyUnique);


          for (let p = 0; p < uniquePatients.length; p++) {
            for (let s = 0; s < uniqueScales.length; s++) {
              for (let a = 0; a < answers.length; a++) {
                if ((answers[a].dataValues.patientId === uniquePatients[p])
                  && (answers[a].dataValues.scaleId === uniqueScales[s])) {

                  //creates object and adds to array

                  let obj = {
                    patient: answers[a].dataValues.Patient,
                    scale: answers[a].dataValues.Scale,
                    question: answers[a].dataValues.question,
                    answer: answers[a].dataValues.answer,
                    dateCreation: answers[a].dataValues.minValue
                  };

                  answersArray.push(obj);
                }
              }
            }
          }

          let ans = [];
          for (let l = 0; l < answersArray.length; l++) {
            if (answersArray[l].question === 'Pontuação' || answersArray[l].question === 'Pontuação Total') {
              ans.push(answersArray[l]);
            }
          }

          log.info('Sending first answers...' + JSON.stringify(ans));
          res.send(ans);
        });

    },
    genericFunctions.error);
  return app;
};