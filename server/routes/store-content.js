const log = require('../support/logger');
const express = require('express');
const app = express();

module.exports = (passport, Model, genericFunctions, pdf) => {

  //Receives info in json format and stores it
  app.post('/json', passport.authenticate('custom-bearer', {failWithError: true}),
    (req, res, next) => {
      log.debug('Entered /store-content/json API. Storing content...');
      let contentType = req.body.contentType;
      log.debug('content type received:', contentType);

      if (!req.body.content) {
        return next({code: 400, message: 'Invalid Parameters in request!'}, req, res, next);
      }

      let author = req.body.content.author;
      log.debug('author received:', author);
      let date = req.body.content.date;
      log.debug('date received:', date);
      //Will also need patient name
      let patientId = req.body.patientId;
      log.debug('patient Id received:', patientId);
      let questions = req.body.content.questions;

      if (!contentType || !author || !date || !patientId || !questions || questions.length === 0) {
        return next({code: 400, message: 'Invalid Parameters in request!'}, req, res, next);
      }

      genericFunctions.storeFile(contentType, author, date, patientId, questions)
        .then(() => {
          res.sendStatus(200);
        }).catch((error) => {
        log.debug(error);
        next(error, req, res, next);
      });
    },
    genericFunctions.error);

  //Receives a pdf, parses it and stores it
  app.post('/pdf', passport.authenticate('custom-bearer', {failWithError: true}),
    (req, res, next) => {
      log.debug('Entered /store-content/pdf API. Parsing content...');
      let contentType = req.body.contentType;
      log.debug('content type received:', contentType);
      let patientId = req.body.patientId;
      log.debug('patient Id received:', patientId);
      let file = req.files.file;
      log.debug('file received:', file);

      if (!contentType || !patientId || !file) {
        return next({code: 400, message: 'Invalid Parameters in request!'}, req, res, next);
      }


      pdf.pdfToJson(file)
        .then((data) => {
          let author = data.author;
          let date = data.date;
          console.log(data);
          genericFunctions.storeFile(contentType, author, date, patientId, data.questions)
            .then(() => {
              res.sendStatus(200);
            }).catch((error) => {
            log.debug(error);
            next(error, req, res, next);
          });
        }).catch((err) => next({code: 400, message: err}, req, res, next));
    },
    genericFunctions.error);

  return app;
};
