const log = require('../support/logger');
const express = require('express');
const app = express();

module.exports = (passport, Model, genericFunctions) => {

  //Returns all patients on bd
  app.get('/', passport.authenticate('custom-bearer', {failWithError: true}),
    (req, res) => {
      log.info('Entered /patients API. Fetching patients...');
      Model.Patient.findAll().then((patients) => {
        log.debug('Fetched Patients:');
        log.debug(JSON.stringify(patients));

        res.send({patients: patients});
      });
    },
    genericFunctions.error);

  //Creates a new patient
  app.post('/', passport.authenticate('custom-bearer', {failWithError: true}),
    (req, res) => {
      log.debug('Entered /patients API. Adding new patient...');

      let validation = genericFunctions.checkRequiredFields(['patient'], req.body);
      if (!validation.validated) {
        res.status(400).send({message: validation.messages[0]});
        return;
      }
      log.debug('Patient received:');
      let patient = req.body.patient;
      log.debug(JSON.stringify(patient));
      //Required fields for validation and error message
      validation = genericFunctions.checkRequiredFields(['name', 'birthDate', 'code'], patient);
      if (!validation.validated) {
        res.status(400).send({message: validation.messages[0]});
        return;
      }
      let isBirthDateValid = genericFunctions.validateDate(patient.birthDate);
      if (!isBirthDateValid) {
        log.error('BirthDate \'' + patient.birthDate + '\' is not valid.');
        res.status(400).send('Invalid Birthdate');
        return;
      }
      Model.Patient.create({
        name: patient.name,
        birthDate: patient.birthDate,
        code: patient.code,
      }).then((patient) => {
        log.info('Added patient. Code -> ' + patient.code);
        log.debug(JSON.stringify(patient));
        res.send(patient);
      }).catch((exception) => {
        res.status(400).send(genericFunctions.getSequelizeErrors(exception));
      }); //End of Patient.create()

    },
    genericFunctions.error);


  //Generates Unique Code
  app.post('/code', passport.authenticate('custom-bearer', {failWithError: true}),
    (req, res) => {
      log.debug('Entered /patients/code API. Generating new code...');
      let code = '';
      if (req.body.patient && req.body.patient.name) {
        code += req.body.patient.name.charAt(0).toUpperCase();
      } else {
        code += genericFunctions.generateCharacter();
      }

      Model.Patient.findAll({attributes: ['code']})
        .then((codes) => {
          let generatedInteger;
          let attempts = 9999;
          let count = 0;
          do {
            generatedInteger = genericFunctions.generateInteger(0, attempts);
            count++;
          }
          while (codes.includes(code + generatedInteger) && count < attempts);
          if (count === attempts) {
            res.sendStatus(500).send({message: "Could not generate code"});
            return;
          }
          res.send({code: code + generatedInteger});
        });

    },
    genericFunctions.error);


  return app;
};
