const log = require('../support/logger');
const express = require('express');
const app = express();

module.exports = (passport, transporter, Model, genericFunctions) => {

    const crypto = require('crypto');

    app.post('/login',
        passport.authenticate('local', {
            failWithError: true
        }), (req, res, next) => {
            res.send(req.user);
        }, (err, req, res, next) => {
            if (!req.body.email || !req.body.password) {
                err.code = 401;
                err.message = 'Missing credentials';
            }
            genericFunctions.error(err, req, res, next);
        });


    /*
     //Route to login
       app.post('/login', function(req, res, next) {
           passport.authenticate('local', function(err, user, info) {
               if (err) {
                   return next(err)
               }
               if (!user) {
                   return res.status(401).json({
                       message: info.message
                   })
               }
               res.json(user);
           })(req, res, function(req, res) {
               res.send(req.user);
           });
       });*/

    //Route to logout
    app.post('/logout', function (req, res) {
        if (!req.body.token) {
            return res.status(400).send({message: "No Token Received!"});
        }

        Model.User.findOne({where: {token: req.body.token}}).then((user) => {
            if (!user) {
                return res.status(404).send({message: "User not Found!"});
            }
            user.token = null;
            user.save().then(() => {
                res.send({message: "OK!"});
            });
        });
    });

    //Route to return platform email
    app.get('/platform/email',
        function (req, res, next) {
            log.debug('Entered get/platform/email API. Getting email...');
            Model.Configuration.findAll().then((emails) => {
                log.debug('Platform email:');
                log.debug(JSON.stringify(emails));


                res.send(emails);
            });
        });


    //Route to password reset
    app.post('/password/reset', function (req, res, next) {
        log.debug('API /password/reset');
        log.debug(req.body.email);

        crypto.randomBytes(48, function (err, buffer) {
            var reset_password_token = buffer.toString('hex');


            Model.User.findOne({where: {email: req.body.email}}).then((user) => {
                if (!user) {
                    let message = 'User not found!';
                    log.debug(message);
                    return res.status(400).send({message: message});
                }

                Model.PasswordReset.create({
                    email: req.body.email,
                    token: reset_password_token
                })
                    .then((data) => {

                        log.debug('Email and token saved on passwordresets table:');
                        log.debug(data);

                        const mailOptions = {
                            from: 'care4value.project@gmail.com',
                            to: req.body.email,
                            subject: 'Care4Value Password Reset',
                            text: 'Please, click the link to reset your password.\n' +
                            process.env.SERVER_URL + '/password-reset/' + reset_password_token,
                            // 'http://46.101.25.53:8080/password-reset/' + reset_password_token,
                            html: '<p>Please, click the link to reset your password.</p>' +
                            '<p><a href="' + process.env.SERVER_URL + '/password-reset/' + reset_password_token + '">password reset link</a></p>'
                            //'<p><a href="http://46.101.25.53:8080/password-reset/' + reset_password_token + '">password reset link</a></p>'

                        };

                        transporter.sendMail(mailOptions, function (err, info) {
                            if (err)
                                log.debug(err)
                            else
                                log.debug(info);
                        });

                        res.sendStatus(200)
                    })
                    .catch((err) => {
                        error = {
                            code: err.code,
                            message: err.sqlMessage
                        };

                        if (!error.message) {
                            error.message = 'Unexpected Error!';
                            error.message = err;
                        }
                        log.debug(error);

                        res.status(500).send(error);
                    });
            });

        });

    });

    //Route to reset password
    app.get('/password-reset/:reset_password_token', function (req, res, next) {

        log.debug('Entered API /password-reset/:reset_password_token');
        log.debug(req.params.reset_password_token);

        //res.redirect('http://165.227.238.36:8080/#/password-reset/' + req.params.reset_password_token);
        res.redirect('http://localhost:8080/#/password-reset/' + req.params.reset_password_token);

        /*
            log.debug('API PASSWORD RESET - get email from table');
            log.debug(req.params.reset_password_token);
            store.getEmailByTokenFromPasswordResets(req.params.reset_password_token).then((emails) => {
              email = emails[0];
              log.debug(emails);
              if(!emails){
                res.status(400).send("Wrong User Id!");
                return;
              }



              if(req.params.reset_password_token == email.email){
                 // res.redirect('http://165.227.238.36:8080');

                store.activateUser(req.params.id).then(() => {
                  res.redirect('http://165.227.238.36:8080');
                  return;
                });

              }else{
                res.status(400).send("Wrong Confirmation Token!");
              }

            });
            */
    });

    //Recovers User Password
    app.put('/recover-password',
        function (req, res, next) {
            if (!req.body.password || !req.body.resetToken) {
                let message = 'Required parameters not found!';
                log.debug(message);
                return res.status(400).send({message: message});
            }
            log.debug('Entered /recover-password API.');

            require("bcrypt").hash(req.body.password, 10).then(function (passwordHash) {
                Model.PasswordReset.findOne({where: {token: req.body.resetToken}}).then((passwordReset) => {
                    if (!passwordReset) {
                        let message = 'Recuperation token not found!';
                        log.debug(message);
                        return res.status(400).send({message: message});
                    }
                    log.debug('Updating User ' + passwordReset.email + 'with hash of password ' + req.body.password);
                    Model.User.update({password: passwordHash},
                        {where: {email: passwordReset.email}}).then((count) => {

                        Model.PasswordReset.destroy({where: {email: passwordReset.email}}).then(() => {
                            res.send({message: 'Password Changed'});
                        }); //End of PasswordReset.destroy()
                    }); //End of User.Update()
                }); //End of PasswordReset.findOne()
            }); //End of require("bcrypt").hash()
        });

    return app;
};
