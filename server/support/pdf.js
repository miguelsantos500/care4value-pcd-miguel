const fs = require('fs');
const path = require('path');
const inspect = require('eyes').inspector({maxLength: 20000});
const log = require('../support/logger');
PDFParser = require("pdf2json");
let genericFunctions;

const getPdfText = (filePath) => {


  return new Promise((resolve, reject) => {
    let pdfParser = new PDFParser(this, 1);

    pdfParser.on("pdfParser_dataError", errData => reject(errData.parserError));
    pdfParser.on("pdfParser_dataReady", pdfData => {
      let content = pdfParser.getRawTextContent();
      content = content.split(/\r?\n/);
      //Creates a new array without ''
      content = content.reduce((newArray, line) => {
        if (line !== '') newArray.push(line);
        return newArray;
      }, []);
      log.debug('Raw PDF Content (Text)', content);

      resolve(content);
    });
    pdfParser.loadPDF(filePath);
  });

};
const getPdfOcr = (filePath) => {
  return new Promise((resolve, reject) => {
    reject('Can\'t read from image!');
    return;

    //These 'requires' should be at the beginning of the file,
    //But this code isn't being used, so
    const pdf_extract = require('./pdf-extract');
    const parseString = require('xml2js').parseString;

    const options = {
      type: 'ocr',  // extract the actual text or ocr in the pdf file
      ocr_flags: [
        '-psm 6',
        '-l por',       // use a custom language file
      ]
    };
    const processor = pdf_extract(filePath, options, (err) => {
      if (err) {
        return reject(err);
      }
    });
    processor.on('complete', (data) => {
      // inspect(data.text_pages, 'extracted text pages');
      let content = '';
      const xml = data.text_pages[0];
      parseString(xml, function (err, result) {
        let body = result.html.body[0];
        let mainDiv = body.div[0].div[0];
        let lines = mainDiv.p[0].span;

        const parsedLines = [];

        for (let i = 0; i < lines.length; i++) {
          let words = lines[i].span;
          const parsedWords = [];
          for (let j = 0; j < words.length; j++) {
            let word = words[j];
            let positions = word['$'].title.split(' ');
            positions.splice(0, 1);
            let parsedWord = {
              content: word._,
              positions: positions
            };
            parsedWords.push(parsedWord);
          }
          parsedLines.push(parsedWords);
        }
        log.info(JSON.stringify(parsedLines));

        const headers = []
        let totalDisagree;
        let disagree;
        let agree;
        let totalAgree;
        let headerIndex = null;
        parsedLines.forEach((line, i) => {
          if (hasHeaders(line)) {
            headers.push(line[0]);
            headers.push(line[1]);
            headers.push(line[2]);
            headers.push(line[3]);
            headerIndex = i;
          }
        });
        if (!headerIndex) {
          throw "No Header Index Error!";
        }
        if (parsedLines[headerIndex + 1][0].content.includes('totalmente')) {
          headerIndex += 1;
        }
        log.info(`Header Index: ${headerIndex}`);
        const choices = [];
        const minHeader = Number(headers[0].positions[0]);
        const maxHeader = Number(headers[3].positions[0]);

        for (let i = headerIndex + 1; i < parsedLines.length; i++) {
          let line = parsedLines[i];
          let chosen = false;
          for (let j = 0; j < line.length; j++) {
            if (chosen) break;
            let word = line[j];
            log.info(word);
            log.warn(headers);
            let horizontal = Number(word.positions[0]);
            if (horizontal >= minHeader && horizontal <= maxHeader) {
              let choice = {min: 999999, position: -1};
              for (let k = 0; k < headers.length; k++) {
                let header = headers[k];
                let diff = Math.abs(Number(header.positions[0]) - horizontal);
                log.error(diff);
                if (diff < choice.min) {
                  choice.min = diff;
                  choice.position = k;
                }
              }
              if (choice.position !== -1) {
                choices.push(choice);
                chosen = true;
              }
            }
          }
        }
        log.info(choices);
        return resolve(choices);

      });
    });
    processor.on('error', (err) => {
      inspect(err, 'error while extracting pages');

      return reject(err);
    });

  });
};

const hasHeaders = (array) => {
  let count = 0;
  array.forEach((o => {
    let content = o.content;
    if (content && (content.includes('Discordo') || content.includes('Concordo') ||
      content.includes('discordo') || content.includes('concordo') ||
      content.includes('Discord') || content.includes('Concord'))) {
      count++;
    }
  }));
  return count === 4;
};
const afterPdfParse = (data) => {
  //Checks if you have the required data
  let parsedData = parse(data);
  if (parsedData !== false) {
    return {result: true, data: parsedData};
  }
  let message = 'Required data not found!';
  log.warn(message);
  return {result: false, error: message};
};


const parse = (data) => {
  if (!data || !Array.isArray(data)) {
    log.warn('No Data Passed!');
    return false;
  }
  const specialWords = [
    {name: 'date', text: 'Data'},
    {name: 'author', text: 'Registado por'},
    {name: null, text: 'Turno'},
    {name: null, text: 'Pontuação Total'}];

  const parsedData = {questions: []};

  /////// SPECIAL PHRASES SECTION //////
  specialWords.forEach((specialObject) => {
    let specialWord = specialObject.text;
    let search = data.filter((word) => word.toLowerCase().includes(specialWord.toLowerCase()));
    let result = null;
    search.forEach((found) => {
      found = found.replace(specialWord, '');

      //If it is of type date
      if (specialWord === 'Data') {
        //Checks if is a valid date
        if (genericFunctions.validateDate(found)) {
          result = found;
        } else {
          if (result === null) {
            log.warn('No Valid Date! Trying to Create Date with only Time...');
            let generated = genericFunctions.getDateFromTime(found);
            if (genericFunctions.validateDate(generated)) {
              result = generated;
              log.debug(result);
            } else {
              log.warn('Failed trying to Create Date with only Time!');
            }
          }
        }
      } else {
        //If there is no previously found value
        if (result === null) {
          result = found;
        }
      }
      //Removes found special phrases
      data.splice(data.findIndex((word) => word.toLowerCase().includes(specialWord.toLowerCase())), 1);
    });

    if (specialObject.name !== null) {
      parsedData[specialObject.name] = result;
    } else {
      parsedData.questions.push({question: specialWord, answer: result});
    }
  });
  /////// SPECIAL PHRASES SECTION - END //////
  /////// NORMAL QUESTIONS //////

  let title = '[PLACHOLDER]';

  let buffer = '[PLACHOLDER]';

  let value = '';
  //data.splice(data.findIndex((line) => line.toLowerCase().includes(('-----page')), 1))

  log.info(data, data.length);


  //Creates a new array without ----page---- breaks
  data = data.reduce((newArray, line) => {
    if (!line.toLowerCase().includes('-----page')) newArray.push(line);
    return newArray;
  }, []);

  log.info(data, data.length);


  for (let i = 0; i < data.length; i++) {
    let line = data[i];
    /*
    *  To ignore the values after this:
    * 'Pontuação',
    * '0 a 5 - Baixo risco de Institucionalização (pontuação social boa)',
    * '6 a 9 - Siutação Intermédia',
    * '10 - Alto risco de Institucionalização ( Deterioro Social severo)',
     */
    if (line.toLowerCase().includes('pontuação')) {
      //Stores previous iteration
      // if (title !== '[PLACHOLDER]') parsedData.questions.push({question: title, answer: value});
      break;

      /*
       *  For this case:
       *  --> previous stored data (title, value) <--<-- STORES IT
       * 'Situação Familiar', --> buffer
       * 'Situação Familiar', --> line
       * 'Vive sozinho, com os filhos',
      */
    } else if (line.includes(buffer) && buffer !== '') {
      //Stores previous iteration
      if (title !== '[PLACHOLDER]') parsedData.questions.push({question: title, answer: value});

      value = '';
      //sets new title
      title = buffer;
      //Removes title part and stores in buffer
      buffer = line.replace(title, '');

      /*
       *  For this case:
       *  --> previous stored data (title, value) <--<-- STORES IT
       * 'Orientação - Em que dia do mês estamos?', --> buffer
       * 'Orientação - Em que dia do mês', --> line
       * 'estamos?Errou', data[i+1]
      */
    } else if (buffer.includes(line) && buffer !== '') {
      i++;
      line = line + ' ' + data[i];
      if (title !== '[PLACHOLDER]') parsedData.questions.push({question: title, answer: value});
      value = '';
      //sets new title
      title = buffer;
      //Removes title part and stores in buffer
      buffer = line.replace(title, '');

      /*
       *  This saves line by line until a new title appears and is caught above:
       * 'Apoio da Rede Social', --> current title
       * 'Recebe apoio social formal', --> buffer
       * 'suficiente (Centro Dia,', --> line
       * 'Ajudnte Familiar, vive num La',
       * 'Pontuação',
       *
       * variable 'value' before this iteration: 'Recebe apoio social formal'
       * variable 'value' after this iteration: 'Recebe apoio social formal suficiente (Centro Dia,'
      */
    } else {
      value += ' ' + buffer;
      buffer = line;
      log.debug(buffer);
    }
  }
  if (title !== '[PLACHOLDER]') parsedData.questions.push({question: title, answer: buffer});

  log.info(parsedData);
  if (!parsedData.author || !parsedData.date || parsedData.questions.length === 0) return false;
  return parsedData;
};

module.exports = (gf) => {
  const m = {};
  genericFunctions = gf;

  m.pdfToJson = (file) => {

    return new Promise((resolve, reject) => {

      //Store File in tmp folder
      let filePath = path.join('tmp', genericFunctions.generateFileName() + '.pdf');

      fs.writeFile('./' + filePath, file.data, (err) => {
        console.log(err);
        if (err) return reject(err);

        filePath = path.join(process.cwd(), filePath);

        let promise = getPdfText(filePath)
          .then((data) => afterPdfParse(data))
          .catch((error) => {
            let message = 'Error getting text from pdf!';
            log.warn(message, JSON.stringify(error) !== '{}' ? JSON.stringify(error) : error);
            return {result: false, error: message};
          });
        promise = promise.then((result) => {
          if (result.result === false) {
            log.debug('Trying OCR.');
            return getPdfOcr(filePath)
              .then((data) => afterPdfParse(data))
              .catch((error) => {
                let message = 'Error getting OCR from pdf!';
                log.warn(message, JSON.stringify(error) !== '{}' ? JSON.stringify(error) : error);
                return {result: false, error: message};
              });
          }
          return result;
        });
        promise
          .then((result) => {
            //Delete File from tmp folder

            fs.unlink(filePath, (err) => {
              if (err) log.warn(err);
            });

            if (result.result === false) {
              let message = 'Failed to parse PDF';
              log.error(message);
              return reject(result.error);
            }
            log.error('PDF parsed successfully.');
            return resolve(result.data);
          })
          .catch((error) => {
            let message = 'Failed to parse PDF';
            log.error(message);
            return reject(error);
          });
      });

    });
  };


  return m;
};
