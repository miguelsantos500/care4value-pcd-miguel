module.exports = require('tracer').colorConsole(
    {
        format: "{{timestamp}} [{{title}}] [{{file}}:{{line}}]: {{message}}",
        dateformat: "HH:MM:ss.L",
        preprocess :  function(data){
            data.title = data.title.toUpperCase();
        }
    });