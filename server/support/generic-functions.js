const log = require('./logger');
const validator = require('validator');
const random = require("random-js")();
const {DateTime} = require('luxon');

module.exports = (Model) => {
  const m = {};

  m.error = (err, req, res, next) => {
    //if someone threw an error

    let status = !err.code ? 500 : err.code;
    let message = !err.message ? err : err.message;
    if (!(typeof message === 'string' || message instanceof String)) {
      message = JSON.stringify(message);
    }

    log.error('Error! Code: ' + status + ', Message: ' + message);

    res.status(status).send({message: message});
  };
  m.checkRequiredFields = (expected, actual) => {
    let toReturn = {validated: true, messages: []};
    for (let i = 0; i < expected.length; i++) {
      let key = expected[i];
      if (!actual[key]) {
        toReturn.validated = false;
        let message = 'Missing Parameter: ' + key;
        log.warn(message);
        toReturn.messages.push(message);
      }
    }
    return toReturn;
  };
  m.validateDate = (string) => {
    //Forces that passed parameter is a string.
    string = string + '';
    log.debug('Date to validate: ' + string);
    let date = validator.toDate(string);
    return date !== null;
  };
  m.getDateFromTime = (timePart, datePart = null) => {
    //Forces that passed parameter is a string.
    datePart = DateTime.local().toFormat('yyyy-MM-dd');
    return DateTime.fromFormat(datePart + 'T' + timePart, 'yyyy-MM-ddTHH:mm:ss').toString();
  };

  m.getSequelizeErrors = (exception) => {
    const errors = [];
    for (let i = 0; i < exception.errors.length; i++) {
      errors.push(exception.errors[i].message);
    }
    log.debug(errors);
    return errors;
  };

  m.generateCharacter = () => {
    return random.string(1, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ');
  };
  m.generateInteger = (min, max) => {
    return random.integer(min, max);
  };
  m.generateFileName = () => {
    return random.uuid4();
  };

  m.storeFile = (contentType, author, date, patientId, answers) => {
    return new Promise((resolve, reject) => {

      //Checks if there is already a submission for that patient/date
      Model.Answer.findAll({where: {date: date, patientId: patientId}}).then((fetchedAnswers) => {
        if (fetchedAnswers.length !== 0) {
          reject({code: 400, message: 'Patient already has answers for that date!'});
          return;
        }
        //Tries to GET Scale
        return Model.Scale.findOne({where: {name: contentType}});
      }).then((scale) => {
        log.debug('Fetched scales:', JSON.stringify(scale));
        if (!scale) {
          return reject({code: 500, message: '\'' + contentType + '\' Not found in DB!'});
        }

        //Checks if input has all required answers

        Model.Question.findAll({
          include: [{
            model: Model.Scale,
            where: {id: scale.id}
          }]
        }).then((questions) => {
          for (let i = 0; i < questions.length; i++) {
            let foundQuestion = answers.filter((a) => a.question === questions[i].name);
            if (foundQuestion.length < 1) {
              log.error(`Question '${questions[i].name}' not found in input answers!`);
              return reject({code: 500, message: 'Some questions not found in input!'});
            }
          }


          let promises = [];
          for (let i = 0; i < answers.length; i++) {
            //Tries to GET each question for given Scale
            promises.push(Model.Question.findOne({
              where: {name: answers[i].question},
              include: [{
                model: Model.Scale,
                where: {id: scale.id}
              }]
            }).then((question) => {
              //log.debug(JSON.stringify(question));
              if (!question) {
                log.error('Question Not found in DB! Questions:' + JSON.stringify(answers));
                reject({code: 500, message: 'Question Not found in DB!'});
                return;
              }
              return Model.ScaleQuestion.findOne({
                where: {questionId: question.id, scaleId: scale.id}
              }).then((scaleQuestion) => {
                return {id: question.id, name: question.name, scaleQuestionId: scaleQuestion.id};
              });

            }));
          }
          Promise.all(promises).then((queryQuestions) => {
            //log.debug(JSON.stringify(queryQuestions))
            //After having question ids and scale ids, time to insert answers!
            let insertAnswers = [];

            for (let l = 0; l < answers.length; l++) {
              let question = queryQuestions.find((q) => {
                return q.name === answers[l].question;
              });
              insertAnswers.push({
                scaleQuestionId: question.scaleQuestionId,
                author: author,
                date: date,
                patientId: patientId,
                question: answers[l].question,
                answer: answers[l].answer,
                groupTotal: answers[l].groupTotal ? answers[l].groupTotal : null,
                questionId: question.id,
                scaleId: scale.id,
              });
            }
            log.debug('Inserting the following answers:', JSON.stringify(answers));
            return Model.Answer.bulkCreate(insertAnswers);
          }).then(() => {
            resolve();
          });
        });
      }).catch(error => {
        reject(error);
      });
    });

  };
  return m;
};

