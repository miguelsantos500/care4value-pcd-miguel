module.exports = function (Model) {
  module = {};

  const passport = require('passport');

  const LocalStrategy = require('passport-local').Strategy;
  const BearerStrategy = require('passport-http-bearer').Strategy;
  const CustomStrategy = require('passport-custom').Strategy;

  const crypto = require('crypto');

  const bcrypt = require("bcrypt");


  //When User logs in
  passport.use(new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password'
  }, (email, password, done) => {
    console.log('Received: ' + email + ', ' + password);

    Model.User.findOne({where: {email: email}}).then(user => {
      if (user === null || user === undefined) {
        return done({
          code: 401,
          message: 'Incorrect credentials'
        });
      }

      if (user.blocked) {
        let message = 'This account has been blocked!';

        console.log('Security error: ');
        console.log(message);

        return done({
          code: 401,
          message: message
        });
      }

      console.log(user.dataValues);

      bcrypt.compare(password, user.password).then((res) => {
        if (!res) {
          console.log('Invalid password!');
          return done({
            code: 401,
            message: 'Incorrect credentials'
          });
        }
        //generates session token and saves it
        crypto.randomBytes(48, (err, buffer) => {
          user.token = buffer.toString('hex');
          user.save()
            .then(r => done(null, {
              id: user.id,
              name: user.name,
              email: user.email,
              role: user.role,
              token: user.token
            }))
            .catch(err => done(err));
        });

      });
    });
  }));

  //When User is Already Logged In
  passport.use('custom-bearer', new CustomStrategy(
    function (req, done) {
      let token = req.token;
      if (!token) {
        return done({
          code: 401,
          message: "No Token Received!"
        });
      }
      Model.User.findOne({where: {token: token}}).then(user => {
        console.log(JSON.stringify(user));
        console.log(token);
        return user ? done(null, user, {
          scope: 'all'
        }) : done({
          code: 401,
          message: "Incorrect credentials"
        });
      })
        .catch(err => {
          console.log(err);
          return done(err);
        });
    }
  ));

  passport.use('admin', new CustomStrategy(
    (req, done) => {
      let token = req.token;
      if (!token) {
        return done({
          code: 401,
          message: "No Token Received!"
        });
      }
      Model.User.findOne({where: {token: token}}).then((user) => {
        console.log('USER:');
        console.log(JSON.stringify(user));
        if (!user) {
          return done({
            code: 401,
            message: "Incorrect credentials"
          });
        }
        if (user.role !== 'Administrator') {
          return done({
            code: 401,
            message: "User is not Admin!"
          });
        }
        return done(null, user);
      });
    }
  ));


  passport.serializeUser((user, done) => {
    done(null, user);
  });

  passport.deserializeUser((user, done) => {
    done(null, user);
  });


  module.initMiddleware = (server) => {
    server.use(passport.initialize());

  };
  module.getPassport = () => {
    return passport;
  };
  return module;
};