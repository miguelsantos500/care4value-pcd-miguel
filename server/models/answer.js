'use strict';
module.exports = (sequelize, DataTypes) => {
  const Answer = sequelize.define('Answer', {
    scaleQuestionId: DataTypes.UUID,
    patientId: DataTypes.UUID,
    author: DataTypes.STRING,
    date: DataTypes.DATE,
    question: DataTypes.STRING,
    answer: DataTypes.STRING,
    groupTotal: DataTypes.INTEGER,
    questionId: DataTypes.INTEGER,
    scaleId: DataTypes.INTEGER,
  }, {});
  Answer.associate = function (models) {
    // associations can be defined here
  };
  return Answer;
};