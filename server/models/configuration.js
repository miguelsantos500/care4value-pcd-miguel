'use strict';
module.exports = (sequelize, DataTypes) => {
  const Configuration = sequelize.define('Configuration', {
    platform_email: DataTypes.STRING,
    platform_email_properties: DataTypes.STRING
  }, {});
  Configuration.associate = function(models) {
    // associations can be defined here
  };
  return Configuration;
};