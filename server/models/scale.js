'use strict';
module.exports = (sequelize, DataTypes) => {
  const Scale = sequelize.define('Scale', {
    name: DataTypes.STRING,
  }, {});
  Scale.associate = function(models) {
    //Scale.belongsToMany(models.question, { through: models.scaleQuestion, as: 'question', foreignKey: 'scaleId' });
    // associations can be defined here
  };
  return Scale;
};