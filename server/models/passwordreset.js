'use strict';
module.exports = (sequelize, DataTypes) => {
  const PasswordReset = sequelize.define('PasswordReset', {
    email: DataTypes.STRING,
    token: DataTypes.STRING
  }, {});
  PasswordReset.associate = function(models) {
    // associations can be defined here
  };
  return PasswordReset;
};