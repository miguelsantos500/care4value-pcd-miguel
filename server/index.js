const log = require('./support/logger');

const allConfigs = require('../config/custom-config');
let customConfigs = {};
if (allConfigs.env) {
  customConfigs = allConfigs[allConfigs.env];
} else {
  log.warn('No Configuration Found!');
}
process.env.SERVER_URL = customConfigs.SERVER_URL || "http://localhost:7555";

const express = require('express');
const app = express();

//Additional routing requiring
const fileUpload = require('express-fileupload');
app.use(fileUpload());
const bodyParser = require('body-parser');
app.use(bodyParser.json());

const bearerToken = require('express-bearer-token');
app.use(bearerToken());


//Middleware to allow CORS and HTTP Methods
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  if (req.method === 'OPTIONS') {
    log.debug('Options request.')
  } else {

    log.info(('---NEW REQUEST---'));
    log.info('REQUEST ', req.method, req.originalUrl);
    log.info('QUERY   ', JSON.stringify(req.query));
    log.info('BODY    ', JSON.stringify(req.body));
  }
  next();
});

let Model = require('./db-connection')(false);


//Implementation of routes security. Needs access to DB
const security = require("./security")(Model);
security.initMiddleware(app);
const passport = security.getPassport();
/*
const session = require('express-session');
const SequelizeStore = require('connect-session-sequelize')(session.Store);
const myStore = new SequelizeStore({
  db: Model.db.sequelize
});
app.use(session({
  secret: 'finalProject2018',
  store: myStore,
  saveUninitialized:true,
  resave: false, // we support the touch method so per the express-session docs this should be set to false
  proxy: false // if you do SSL outside of node.
}));
myStore.sync();

*/
const nodemailer = require('nodemailer');
const transporter = nodemailer.createTransport({
  host: 'smtp.gmail.com',
  port: 587,
  secure: false,
  requireTLS: true,
  auth: {
    user: 'care4value.project@gmail.com',
    pass: 'finalProject2018'
  }
});

const genericFunctions = require('./support/generic-functions')(Model);
const pdf = require('./support/pdf')(genericFunctions);

app.use('/', require('./routes/generic')(passport, transporter, Model, genericFunctions));

app.use('/users/', require('./routes/users')(passport, Model, genericFunctions));

app.use('/patients/', require('./routes/patients')(passport, Model, genericFunctions));

app.use('/answers/', require('./routes/answers')(passport, Model, genericFunctions));

app.use('/store-content', require('./routes/store-content')(passport, Model, genericFunctions, pdf));

app.post('/testing', (req, res) => {
  if (!req.body.password) {
    return res.status(401).send({message: "No Password Received!"});
  }
  if (req.body.password !== 'finalProject2018') {
    return res.status(401).send({message: "Incorrect Password!"});
  }
  if (req.body.testing === null || req.body.testing === undefined) {
    return res.status(400).send({message: "Did not send 'testing' parameter!"});
  }
  Object.assign(Model, require('./db-connection')(req.body.testing));
  return res.send({message: 'Done!'});

});
const server = app.listen(7555, () => {
  log.debug('Server running on http://localhost:7555')
});

module.exports.server = server; // for testing