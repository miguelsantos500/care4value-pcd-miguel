module.exports.env = process.env.CLIENT_ENVIRONMENT || 'local';
module.exports.local = {
	SERVER_URL: 'http://localhost:7555',
	CLIENT_URL: 'http://localhost:8080',
};
module.exports.dev = {
	SERVER_URL: 'http://178.128.47.243:7555',
	CLIENT_URL: 'http://178.128.47.243:8080',
};
module.exports.prod = {
	SERVER_URL: 'http://xxxxxxxx:7555',
	CLIENT_URL: 'http://xxxxxxxxxxx:8080',
};