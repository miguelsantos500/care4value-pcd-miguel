# Instalation

## Setup Virtual Machine (Example using AWS)

- Create a ubuntu machine:

```
#AWS Only: Security Group
On AWS Console, go to Network & Security -> Security Groups;
Click Create Security Group
Set Name and Description;
Add 2 Rules:
Custom TCP Rule, Port Range -> 8080, Source -> Anywhere
Custom TCP Rule, Port Range -> 7555, Source -> Anywhere  

Click Create

Then, add this security group to your created machine

```

- Allow Login with password:

```
#Login with .pem via ssh 
ssh -i "care4value-pcd.pem" ubuntu@ec2-18-216-147-243.us-east-2.compute.amazonaws.com
#Edit sshd_config , look for PasswordAuthentication No and change it to PasswordAuthentication Yes
sudo vi /etc/ssh/sshd_config
sudo /etc/init.d/ssh restart
#Then connect to test if all is well.
```
- Change password:
```
passwd ubuntu
```
- Change hostname:
https://aws.amazon.com/premiumsupport/knowledge-center/linux-static-hostname/
```
#Update the /etc/hosts file on your Ubuntu Linux instance with the new hostname 
sudo vim /etc/hosts
#"127.0.0.1    hostname" to "127.0.0.1    care4value.pcd"

#Then,
sudo vim /etc/hostname
#and replace with care4value.pcd

#run the Linux hostname command and specify the new hostname 
sudo hostname care4value.pcd

#Finally, reboot.
```


## Project Software Requirements
### Install NodeJS/NPM

```
curl -sL https://deb.nodesource.com/setup_9.x | sudo -E bash -
sudo apt-get install -y nodejs
```


### Install MySQL

```
sudo apt-get update
sudo apt-get install mysql-server
#Set MySQL Root Password - Has to be the same that will be used in your server conf file
mysql_secure_installation

#Create Database
mysql -u root -e "create database care4value" -p
```



## Install Project
### Create care4value User

```
#as root
useradd care4value
usermod -aG sudo care4value

```

### Get Project Code

```
#as root
mkdir /opt/care4value-pcd
chown care4value /opt/care4value-pcd
#as care4value
cd /opt
#care4value@bitbucket.org -> the first part is your username, in this case, care4value
git clone https://care4value@bitbucket.org/miguel_santos_/care4value-pcd.git

```

### Install Server

```
cd /opt/care4value-pcd/server
npm install

```
#### Populate Database

```
cd /opt/care4value-pcd/server
node_modules/.bin/sequelize db:migrate:undo:all
node_modules/.bin/sequelize db:migrate
node_modules/.bin/sequelize db:seed:all
```


### Install Client

```
cd /opt/care4value-pcd/server
npm install

```

### Change Environment to what you want

```
#Example for DEV Environment
sudo vi /opt/care4value-pcd/config/custom-config.js
#Change First list to 'dev'

```

### Install pm2

```
sudo npm install pm2 -g

```

## Run Project
### Start Server for the first time
```
cd /opt/care4value-pcd/server
pm2 start --name server npm -- start
```
### Start Client for the first time
```
cd /opt/care4value-pcd/client
pm2 start --name client npm -- start
```

### Stop Server and Client
```
#From anywhere:
pm2 stop all
```
### Start Server and Client again
```
#From anywhere:
pm2 start all
```