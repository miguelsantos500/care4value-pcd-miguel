import XLSX from 'xlsx';

const createSummarizedXlsx = (firstAnswers, answers) => {


  let first = {
    data: [
      ["Patient", "Scale", "Date", "Score"],
    ]
  };

  if (firstAnswers.length) {
    let line = [];
    for (let i = 0; i < firstAnswers.length; i++) {
      let l = firstAnswers[i];
      line = [l.patient.name, l.scale.name, l.dateCreation, l.answer];
      first.data.push(line);
    }

  }

  let workbook = XLSX.utils.book_new();

  let firstWorksheet = XLSX.utils.aoa_to_sheet(first.data);
  XLSX.utils.book_append_sheet(workbook, firstWorksheet, "Answers");

  const header = ["Patient", "Date", "Question", "Answer"];

  const content = {
    barthel: {data: [header], sheet: 'Barthel answers'},
    braden: {data: [header], sheet: 'Braden answers'},
    gijon: {data: [header], sheet: 'Gijon answers'},
    mna: {data: [header], sheet: 'MNA answers'},
    mns: {data: [header], sheet: 'MNS answers'},
  };

  if (answers.length) {
    let line = [];
    for (let b = 0; b < answers.length; b++) {

      let obj = answers[b];

      //Shouldn't be checking with ScaleId, but with Scale Name.
      switch (obj.scale.name) {
        case 'Barthel':
          line = [obj.patient.name, obj.date, obj.question, obj.answer];
          content.barthel.data.push(line);
          break;
        case 'Braden':
          line = [obj.patient.name, obj.date, obj.question, obj.answer];
          content.braden.data.push(line);
          break;
        case 'Gijon': //gijon
          line = [obj.patient.name, obj.date, obj.question, obj.answer];
          content.gijon.data.push(line);
          break;
        case 'MNA': //mna
          line = [obj.patient.name, obj.date, obj.question, obj.answer];
          content.mna.data.push(line);
          break;
        case 'MNS': //mns
          line = [obj.patient.name, obj.date, obj.question, obj.answer];
          content.mns.push(line);
          break;
      }
    }
  }

  let keys = Object.keys(content);
  keys.forEach((k) => {
    let sheet = XLSX.utils.aoa_to_sheet(content[k].data);
    XLSX.utils.book_append_sheet(workbook, sheet, content[k].sheet);
  });

  XLSX.writeFile(workbook, 'Answers.xlsx');


};


export default {createSummarizedXlsx}