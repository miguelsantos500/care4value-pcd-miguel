const arrays = {
  Barthel: ["1-Beber por uma chávena", "2- Comer", "3- Vestir a parte superior do corpo",
    "4- Vestir a parte inferior do corpo", "6- Toalhete pessoal", "7- Lavar-se ou tomar banho",
    "8- Controlo da urina", "9- Controlo dos intestinos", "10- Sentar-se e levantar-se da cadeira",
    "11- Sentar-se e levantar-se da sanita", "12- Entrar e sair da banheira ou duche", "13- Andar 50 metros em piso plano",
    "14- Subir e descer um lanço de escadas", "15- SE NÃO ANDAR, impulsionar ou puxar uma cadeira de rodas",
    "Pontuação", "Interpretação do resultado", "Ações a realizar"],
  Braden: ["Percepção Sensorial (capacidade de reacção significativa ao desconforto)",
    "Humidade", "Actividade", "Mobilidade", "Nutrição", "Fricção e Forças de Deslizamento",
    "Pontuação", "Interpretação do resultado", "Ações a realizar"],
  Gijon: ["Turno", "Situação Familiar", "Relações e Contatos Sociais", "Apoio da Rede Social", "Pontuação Total"],
  MNA: ["A. Nos últimos três meses houve diminuição da ingestão alimentar devido a perda de apetite, problemas digestivos ou dificuldade para mastigar ou deglutir?",
    "B. Perda de peso nos últimos 3 meses", "C. Mobilidade",
    "D. Passou por algum stress psicológico ou doença aguda nos últimos três meses?",
    "E. Problemas Psocológicos/Neurológicos", "F1. Índice de Massa Corporal (IMC = peso [KG] / estatura [m]2)",
    "F2. Circunferência da perna (CP) em cm", "Idade", "Pontuação Total"],
  MNS: ["Turno", "Orientação - Em que ano estamos?", "Orientação - Em que mês estamos?",
    "Orientação - Em que dia do mês estamos?", "Orientação - Em que dia da semana estamos?",
    "Orientação - Em que estação do ano estamos?", "Orientação - Em que país estamos?",
    "Orientação - Em que distrito vive?", "Orientação - Em que terra vive?",
    "Orientação - Em que casa estamos?", "Orientação - Em que andar estamos?",
    "Retenção - Pêra", "Retenção - Gato", "Retenção - Bola", "Atenção e Cálculo - 27",
    "Atenção e Cálculo - 24", "Atenção e Cálculo - 21", "Atenção e Cálculo - 18", "Atenção e Cálculo - 15",
    "Evocação - Pêra", "Evocação - Gato", "Evocação - Bola", "Linguagem - Como se chama isto? Relógio",
    "Linguagem - Como se chama isto? Lápis", "Linguagem - O RATO ROEU A ROLHA", "Linguagem - Pega com a mão direita",
    "Linguagem - Dobra ao meio", "Linguagem - Coloca onde deve", "Linguagem - Fechou os olhos",
    "Linguagem - Escrever uma frase inteira", "Habilidade Construtiva - Copiar um desenho", "Pontuação Total"]

};


const validate = (json, questions) => {
  if (!questions) {
    console.error('Questions not configured for given type!');
    return false;
  }

  if (!(json && json.author && json.date && json.questions && json.questions.length === questions.length)) {
    return false;
  }
  for (const bq of questions) {
    let question = json.questions.find(x => x.question === bq);
    if (question === undefined) return false;
    if (question.question === undefined || question.question === null) return false;
    // if (question.answer === undefined || question.answer === null) return false;
  }
  return true;
};

//Type One is for Barthel and Braden
const type1 = (json) => {
  let parsedObject = {};
  parsedObject.questions = [];
  let aux = null;
  for (let i = 0; i < json.length; i++) {
    let arrayToParse = json[i];
    if (arrayToParse.length === 3) {
      switch (arrayToParse[1]) {
        case 'Autor':
          parsedObject.author = arrayToParse[2];
          break;
        case 'Data':
          parsedObject.date = arrayToParse[2];
          break;
        case 'Total do grupo':
          aux.groupTotal = arrayToParse[2];
          break;
        default:
          if (aux != null) {
            parsedObject.questions.push(aux);
          }
          aux = {};
          aux.question = arrayToParse[1];
          aux.answer = arrayToParse[2];
      }
    }
  }
  if (aux != null) {
    parsedObject.questions.push(aux);
  }
  return parsedObject;
};

const type2 = (json) => {
  let parsedObject = {};
  parsedObject.questions = [];

  for (let i = 0; i < json.length; i++) {
    let arrayToParse = json[i];
    if (arrayToParse[0] && arrayToParse.length === 3) {
      switch (arrayToParse[0]) {
        case 'Data': {
          parsedObject.date = arrayToParse[2];
          break;
        }
        case 'Registado por': {
          parsedObject.author = arrayToParse[2];
          break;
        }
        default: {
          parsedObject.questions.push({question: arrayToParse[0], answer: arrayToParse[2]});
        }
      }
    } else if (arrayToParse[1]) {
      parsedObject.questions.push({question: arrayToParse[1], answer: arrayToParse[2]});
    }
  }
  return parsedObject;
};

const type3 = (json) => {
  let parsedObject = {};
  parsedObject.questions = [];
  console.log(JSON.stringify(json));
  return parsedObject;
};

const jsonToContent = (json, type) => {
  let parsedObject;
  switch (type) {
    case 'Barthel':
    case 'Braden':
      parsedObject = type1(json);
      break;
    case 'Gijon':
    case 'MNA':
    case 'MNS':
      parsedObject = type2(json);
      break;
  }

  let validateArray = arrays[type];
  if (!validate(parsedObject, validateArray)) {
    return false;
  }
  return parsedObject;
};


export default {jsonToContent}