import XLSX from 'xlsx';

function fileToJson(file) {
    //if xlsx
    return new Promise((resolve, reject) => {
        let fr = new FileReader();
        fr.onload = (e) => {
            let results,
                data = e.target.result,
                workbook = XLSX.read(btoa(fixData(data)), {type: 'base64', cellDates: true}),
                sheetName = workbook.SheetNames[0],
                worksheet = workbook.Sheets[sheetName];
            results = XLSX.utils.sheet_to_json(worksheet, { header: 1, raw: true });
            //console.log(results);
            resolve(results);
        };
        fr.readAsArrayBuffer(file);
    });
}

function fixData(data) {
    let o = "", l = 0, w = 10240;
    for (; l < data.byteLength / w; ++l) o += String.fromCharCode.apply(null, new Uint8Array(data.slice(l * w, l * w + w)));
    o += String.fromCharCode.apply(null, new Uint8Array(data.slice(l * w)));
    return o;
}


export default {fileToJson}