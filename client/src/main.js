const allConfigs = require('../../config/custom-config');
let customConfigs = {};
if (allConfigs.env) {
  customConfigs = allConfigs[allConfigs.env];
} else {
  console.warn('No Configuration Found!');
}
const serverUrl = customConfigs.SERVER_URL || "http://localhost:7555";


import Vue from 'vue';

//Additional Vue Imports
import VueRouter from 'vue-router';
import BootstrapVue from 'bootstrap-vue';
import axios from 'axios';
import VueAxios from 'vue-axios';
import VueLocalStorage from 'vue-localstorage';
import VueMoment from 'vue-moment';
import moment from 'moment';
import VeeValidate from 'vee-validate';
import MultiLanguage from 'vue-multilanguage';

Vue.use(VueRouter);
Vue.use(BootstrapVue);
Vue.use(VueAxios, axios);
Vue.use(VueLocalStorage);
Vue.use(VueMoment, {
  moment
});

const {localeDefault, multilanguageJSON, validatorJSON} = require('./assets/multilanguage');

//Checks if there already is language set (MultiLanguage does that)
let locale = Vue.localStorage.get('vue-lang');
if (!locale) {
  locale = localeDefault;
}

//Vue.use(VeeValidate);
Vue.use(VeeValidate, {fieldsBagName: 'formFields'});

VeeValidate.Validator.localize(validatorJSON);
const validator = new VeeValidate.Validator();

validator.localize(locale);

Vue.use(MultiLanguage, multilanguageJSON);


import {Validator} from 'vee-validate';

const v = {
  getMessage(/*field, args*/) {
    return 'Choose a valid patient.';
  },
  validate(code, [boolean]) {
    return boolean === true || boolean === 'true';
  }
};

Validator.extend('patientExists', v);


//Other Imports
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import App from './App.vue';

import {eventBus} from './event-bus';


Vue.config.productionTip = false;


//Vue Components
import Dashboard from '@/components/Dashboard';
import Login from '@/components/Login';
import UserManagement from '@/components/UserManagement';
import Profile from '@/components/Profile';
import Upload from '@/components/Upload';
import PasswordReset from '@/components/PasswordReset';
import PatientManagement from '@/components/PatientManagement';


const routes = [{
  path: '/',
  redirect: '/dashboard'
},
  {
    path: '/dashboard',
    name: 'Dashboard',
    component: Dashboard
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/user-management',
    name: 'UserManagement',
    component: UserManagement
  },
  {
    path: '/patient-management',
    name: 'PatientManagement',
    component: PatientManagement
  },
  {
    path: '/profile',
    name: 'Profile',
    component: Profile
  },
  {
    path: '/upload',
    name: 'Upload',
    component: Upload
  }, {
    path: '/password-reset/:resetToken',
    name: 'PasswordReset',
    component: PasswordReset
  }
];

const router = new VueRouter({
  routes: routes
});

import {user, store} from './store/store'


router.beforeEach((to, from, next) => {
  const loggedRoutes = ['Dashboard', 'UserManagement', 'Profile', 'Upload', 'PatientManagement'];
  //if route to go to is one that requires Login
  console.log(to.name);
  if (loggedRoutes.indexOf(to.name) > -1) {
    if (!user.token) {
      console.log('You have to be logged in to go to this route!');
      return next({path: '/login'})
    }
  }
  const adminRoutes = ['UserManagement'];
  //if route to go to is one that requires Login
  if (adminRoutes.indexOf(to.name) > -1) {
    if (user.role !== 'Administrator') {
      console.log('You have to be administrator to go to this route!');
      console.log(vm.$refs);
      //vm.$refs.errorModalRef.show();
      eventBus.$emit('show-error-modal', 'Permission denied: you have to be administrator to go to this route!');
      return next({path: '/'})
    }
  }
  next();
});


const vm = new Vue({
  store,
  render: h => h(App),
  router: router,
  data() {
    return {
      user: user,
      serverUrl: serverUrl
    }
  },
  methods: {
    updateUser(newUser) {
      Object.assign(user, newUser);
    },
  }
}).$mount('#app');
vm.$moment.locale(locale);

