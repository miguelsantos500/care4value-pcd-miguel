const localeDefault = 'en';
module.exports.localeDefault = localeDefault;
module.exports.multilanguageJSON = {
  default: localeDefault,
  en: {

    ////    App Area    ////
    liLanguage: 'Language: \'{language}\'',
    linkLanguageEnglish: 'EN (English)',
    linkLanguagePortuguese: 'PT (Portuguese)',
    linkProfile: 'My Profile',
    linkDashboard: 'Dashboard',
    linkUserManagement: 'User Management',
    linkPatientManagement: 'Patient Management',
    linkUpload: 'Upload',


    ////    Login Area    ////
    loginArea: 'Login Area',
    inputLoginLabel: 'Login Credential',
    inputLoginPlaceholder: 'Your Login Credential',
    inputPasswordLabel: 'Password',
    inputPasswordPlaceholder: 'Your Password',
    buttonLogin: 'Login',
    passwordResetArea: 'Password Reset',
    buttonResetPassword: 'Reset Password',
    buttonIForgot: 'I forgot my password',

    /// Password Reset Area ///
    passwordResetComponent: 'Password Reset',
    passwordResetNewPassword: 'New Password',
    inputPasswordResetComponent: 'Insert new password',
    passwordResetPasswordConfirmation: 'Password Confirmation',
    inputPasswordResetConfirmationComponent: 'Confirm your password',
    passwordResetChangeButton: 'Change',

    ////        UserManagement Area      ////
    headerUserManagement: 'User Management',
    tabUsers: 'Users',
    tabUserAdd: 'Add User',

    ////        Users Area      ////
    headerUsers: 'Users',
    tableUsersName: 'Name',
    buttonRevoke: 'Block',
    buttonGrant: 'Unblock',
    tableUsersActions: 'Actions',
    tableUsersBlocked: 'Blocked',
    blockedYes: 'Yes',
    blockedNo: 'No',
    tableUsersShowDetails: 'Show/Change Details',
    labelinputTableName: 'Name: ',
    labelinputTableEmail: 'Email: ',
    showTableUserDetails: 'Show Details',
    hideTableUserDetails: 'Hide Details',
    tableUserSave: 'Save',

    ////        UserAdd Area      ////
    userAddArea: 'Add user',
    labelUserName: 'Name',
    labelPasswordConfirmation: 'Password Confirmation',
    dropDownButtonAdmin: 'Administrator',
    dropDownButtonResearcher: 'Researcher',
    dropDownButtonHealthProf: 'Health professional',
    linkSaveUser: 'Save',
    dropDownRole: 'Choose users role',

    ////        PatientManagement Area      ////
    headerPatientManagement: 'Patient Management',
    tabPatients: 'Patients',
    tabPatientAdd: 'Add Patient',
    ////        Patients Area      ////
    headerPatient: 'Patients',
    tablePatientsName: 'Name',
    tablePatientsBirthDate: 'Birth Date',
    tablePatientsCode: 'Code',
    tablePatientsShowDetails: 'Show Details',
    ////        PatientAdd Area      ////
    patientAddArea: 'Add Patient',
    labelPatientName: 'Name',
    labelPatientBirthDate: 'Birth Date',
    labelPatientCode: 'Code',
    buttonPatientGenerateCode: 'Generate Code',
    linkSavePatient: 'Save',
    buttonGenerateCode: 'Generate Code',

    ////        Profile Area      ////
    profileArea: 'My Profile',
    inputProfileName: 'Your Name:',
    inputProfileEmail: 'Your Email:',
    inputProfileRole: 'Your Role:',
    buttonTogglePassword: 'Change Password',
    labelInputNewPassword: 'New Password:',
    labelInputNewPasswordConfirmation: 'New Password Confirmation:',
    buttonSaveNewPassword: 'Save New Password',


    ////        Upload Area      ////
    headerUpload: 'Upload Area',
    chooseFile: 'Choose a file...',
    chosenFile: 'Chosen file: ',
    btnRadiosFileType: 'Choose file type to upload',
    btnRadiosFileContentType: 'Choose file content type to upload',
    groupTotal: 'Group Total: ',
    noGroupTotal: 'No Group Total',
    showFileDetails: 'Show File Details',
    patientName: 'Patient Name',
    author: 'Author',
    date: 'Date',
    '\'Barthel\' Not found in DB!': '\'Barthel\' Not found in DB!',
    'Error Parsing File': 'Error Parsing File! Please check your chosen file.',
    'Patient already has answers for that date!': 'Patient already has answers for that date!',
    answersInServer: 'Answers for this Patient and Date stored in the server:',
    'Choose a File': 'Please choose a file.',
    labelPatient: 'Patient',
    noPatients: 'There are no Patients for that search!',


    ////        Answers Area      ////
    tableAnswersPatient: 'Patient',
    tableAnswersQuestion: 'Question',
    tableAnswersAnswer: 'Answer',
    tableAnswersAuthor: 'Author',
    tableAnswersDate: 'Date',
    tableAnswersGroupTotal: 'Group Total',

    ////    Server Translations   ////
    ////*********************************Server Translations***************************************////
    'Network Error': 'Error trying to connect to server!',

    //->UserAdd Area
    'email must be unique': 'That email already exists!',

    //->Profile Area
    'User Updated': 'User Updated Successfully!',
    'Required field': 'There should not be empty fields!',

    //->login area
    'User not found!': 'There is no account with that email address.',
    'Missing credentials': 'Missing credentials.',
    'Incorrect credentials': 'Incorrect credentials.',
    'This account has been blocked!': 'Can\'t loggin: your account has been blocked!',

    //->PatientAdd
    'code must be unique': 'Code already being used. Please choose another code.',

    ////*************************************Client Translations***********************************////

    //->UserAdd Area
    'User Created': 'User Created Successfully!',

    //->PasswordReset Area
    ////        Client Translations     ////
    'Password Changed': 'Password Changed Successfully!',

    //->Users Area
    'User blocked': 'User was blocked!',
    'User unblocked': 'User was unblocked!',
    'File Uploaded Successfully': 'File Uploaded Successfully!',

    //->login area
    'Email sent': 'An Email has been sent to your email with a link for password reset.',


    //->PatientAdd
    'Patient Created': 'Patient Created Successfully!',

    'Generic Error': 'Sorry, something seems to be wrong.'
  },

  pt: {

    ////    App Area    ////
    liLanguage: 'Linguagem: \'{language}\'',
    linkLanguageEnglish: 'EN (Inglês)',
    linkLanguagePortuguese: 'PT (Português)',
    linkDashboard: 'Dashboard',
    linkUsers: 'Utilizadores',
    linkUserManagement: 'Gestão de Utilizadores',
    linkPatientManagement: 'Gestão de Utentes',
    linkProfile: 'O Meu Perfil',
    linkUpload: 'Upload',

    ////    Login Area    ////
    loginArea: 'Área de Login',
    inputLoginLabel: 'Credencial de Login',
    inputLoginPlaceholder: 'A Sua Credencial de Login',
    inputPasswordLabel: 'Palavra-Passe',
    inputPasswordPlaceholder: 'A Sua Palavra-Passe',
    buttonLogin: 'Entrar',

    ////        Profile Area      ////
    passwordResetArea: 'Recuperação de Password',
    buttonResetPassword: 'Recuperar Password',
    buttonIForgot: 'Esqueci-me da password',

    /// Password Reset Area ///
    passwordResetComponent: 'Recuperação de Password',
    passwordResetNewPassword: 'Nova Password',
    inputPasswordResetComponent: 'Insira a nova password',
    passwordResetPasswordConfirmation: 'Confirmação de Password',
    inputPasswordResetConfirmationComponent: 'Confirme a sua password',
    passwordResetChangeButton: 'Guardar',

    ////        Profile Area      ////
    profileArea: 'O Meu Perfil',
    inputProfileName: 'O seu Nome:',
    inputProfileEmail: 'O seu Email:',
    inputProfileRole: 'A sua Função:',
    buttonTogglePassword: 'Mudar a Palavra-Passe',
    labelInputNewPassword: 'Nova Palavra-Passe:',
    labelInputNewPasswordConfirmation: 'Confirmar Nova Palavra-Passe:',
    buttonSaveNewPassword: 'Guardar Nova Palavra-Passe',

    ////        UserManagement Area      ////
    headerUserManagement: 'Gestão de Utilizadores',
    tabUsers: 'Utilizadores',
    tabUserAdd: 'Adicionar Utilizador',

    ////        Users Area      ////
    headerUsers: 'Users',
    tableUsersName: 'Nome',
    buttonRevoke: 'Bloquear',
    buttonGrant: 'Desbloquear',
    tableUsersActions: 'Acções',
    tableUsersBlocked: 'Bloqueado',
    blockedYes: 'Sim',
    blockedNo: 'Não',
    tableUsersShowDetails: 'Mostrar/Alterar Detalhes',
    labelinputTableName: 'Nome: ',
    labelinputTableEmail: 'Email: ',
    showTableUserDetails: 'Mostrar Detalhes',
    hideTableUserDetails: 'Esconder Detalhes',
    tableUserSave: 'Guardar',

    ////        UserAdd Area      ////
    userAddArea: 'Adicionar Utilizador',
    labelUserName: 'Nome',
    labelPasswordConfirmation: 'Confirmação de Password',
    dropDownButtonAdmin: 'Administrador',
    dropDownButtonResearcher: 'Investigador',
    dropDownButtonHealthProf: 'Profissional de saúde',
    linkSaveUser: 'Guardar',
    dropDownRole: 'Escolha a função do utilizador',

    ////        PatientManagement Area      ////
    headerPatientManagement: 'Gestão de Utentes',
    tabPatients: 'Utentes',
    tabPatientAdd: 'Adicionar Utente',
    ////        Patients Area      ////
    headerPatient: 'Utentes',
    tablePatientsName: 'Nome',
    tablePatientsBirthDate: 'Data de Nascimento',
    tablePatientsCode: 'Código',
    tablePatientsShowDetails: 'Mostrar Detalhes',
    ////        PatientAdd Area      ////
    patientAddArea: 'Adicionar Utente',
    labelPatientName: 'Nome',
    labelPatientBirthDate: 'Data de Nascimento',
    labelPatientCode: 'Código',
    buttonPatientGenerateCode: 'Gerar Código',
    linkSavePatient: 'Guardar',
    buttonGenerateCode: 'Gerar Código',

    ////        Upload Area      ////
    headerUpload: 'Área de Carregamento de Ficheiro',
    chooseFile: 'Escolha um ficheiro...',
    chosenFile: 'Ficheiro escolhido: ',
    btnRadiosFileType: 'Escolha um tipo de ficheiro para fazer upload',
    btnRadiosFileContentType: 'Escolha um tipo de ficheiro (conteúdo) para fazer upload',
    groupTotal: 'Total de Grupo: ',
    noGroupTotal: 'Sem Total de Grupo',
    showFileDetails: 'Mostrar Detalhes do Ficheiro',
    patientName: 'Nome do Paciente',
    author: 'Autor',
    date: 'Data',
    '\'Barthel\' Not found in DB!': '\'Barthel\' Não encontrado na BD!',
    'Error Parsing File': 'Erro ao processar o ficheiro! Por favor verifique o ficheiro escolhido.',
    'Patient already has answers for that date!': 'O Utente já tem submissão para essa data!',
    answersInServer: 'Respostas deste Paciente e Data guardadas no servidor:',
    'Choose a File': 'Por favor escolha um ficheiro.',
    labelPatient: 'Utente',
    noPatients: 'Não existem utentes para essa pesquisa!',

    ////        Answers Area      ////
    tableAnswersPatient: 'Paciente',
    tableAnswersQuestion: 'Pergunta',
    tableAnswersAnswer: 'Resposta',
    tableAnswersAuthor: 'Autor',
    tableAnswersDate: 'Data',
    tableAnswersGroupTotal: 'Total de Groupo',

    ////    Server Translations   ////
    'Missing credentials':
      'Campos não preenchidos.',
    'Incorrect credentials':
      'Credenciais Incorretas.',
    'email must be unique':
      'Email já existe!',
    ////*********************************Server Translations***************************************////
    'Network Error':
      'Erro ao tentar contactar o servidor!',

    //->Profile Area
    'User Updated':
      'Utilizador Atualizado com Sucesso!',

    //->UserAdd Area
    'Required field':
      'Não pode haver campos vazios!',

    //->Users Area
    'This account has been blocked!':
      'Não pode fazer login: a sua conta foi bloqueada!',

    //->Login area
    'User not found!':
      'Não existe nenhuma conta com esse endereço de email.',

    //->PatientAdd
    'code must be unique': 'Código já em utilização. Por favor escolha outro código.',


    ////*************************************Client Translations***********************************////

    //->Users Area
    'User Created':
      'Utilizador criado com sucesso!',

    //->PasswordReset Area
    'Password Changed':
      'Password Alterada com Sucesso!',

    //->Users Area
    'User blocked':
      'Utilizador foi bloqueado!',
    'User unblocked':
      'Utilizador foi desbloqueado!',

    //->Login area
    'Email sent':
      'Um Email foi enviado para a sua conta de Email com o link de recuperação de password.',
    'File Uploaded Successfully':
      'Ficheiro submetido!',

    //->PatientAdd
    'Patient Created': 'Utente Criado com Sucesso!',


    'Generic Error': 'Desculpe, algo correu mal.'
  }
}
;

module.exports.validatorJSON = {
  en: {
    messages: {
      //required: (e) => 'The \'' + e + '\' field is required.'
      required: () => 'Required field.',
      confirmed: () => 'Passwords do not match.',
      patientExists: () => 'Choose a valid patient.'
    }
  },
  pt: {
    messages: {
      //required: (e) => 'O campo \'' + e + '\' é obrigatório.'
      required: () => 'Campo obrigatório.',
      confirmed: () => 'As Palavras-Passe não são iguais.',
      patientExists: () => 'Escolha um utente válido.'
    }
  }
};


//Below, are all the messages for vee-validate (our validatorJSON)


/*

! function(e, n) { "object" == typeof exports && "undefined" != typeof module ? module.exports = n() : "function" == typeof define && define.amd ? define(n) : (e.__vee_validate_locale__en = e.__vee_validate_locale__en || {}, e.__vee_validate_locale__en.js = n()) }(this, function() {
    "use strict";
    var e, n = {
        name: "en",
        messages: {
            _default: function(e) { return "The " + e + " value is not valid." },
            after: function(e, n) { var t = n[0]; return "The " + e + " must be after " + (n[1] ? "or equal to " : "") + t + "." },
            alpha_dash: function(e) { return "The " + e + " field may contain alpha-numeric characters as well as dashes and underscores." },
            alpha_num: function(e) { return "The " + e + " field may only contain alpha-numeric characters." },
            alpha_spaces: function(e) { return "The " + e + " field may only contain alphabetic characters as well as spaces." },
            alpha: function(e) { return "The " + e + " field may only contain alphabetic characters." },
            before: function(e, n) { var t = n[0]; return "The " + e + " must be before " + (n[1] ? "or equal to " : "") + t + "." },
            between: function(e, n) { return "The " + e + " field must be between " + n[0] + " and " + n[1] + "." },
            confirmed: function(e) { return "The " + e + " confirmation does not match." },
            credit_card: function(e) { return "The " + e + " field is invalid." },
            date_between: function(e, n) { return "The " + e + " must be between " + n[0] + " and " + n[1] + "." },
            date_format: function(e, n) { return "The " + e + " must be in the format " + n[0] + "." },
            decimal: function(e, n) { void 0 === n && (n = []); var t = n[0]; return void 0 === t && (t = "*"), "The " + e + " field must be numeric and may contain " + (t && "*" !== t ? t : "") + " decimal points." },
            digits: function(e, n) { return "The " + e + " field must be numeric and exactly contain " + n[0] + " digits." },
            dimensions: function(e, n) { return "The " + e + " field must be " + n[0] + " pixels by " + n[1] + " pixels." },
            email: function(e) { return "The " + e + " field must be a valid email." },
            ext: function(e) { return "The " + e + " field must be a valid file." },
            image: function(e) { return "The " + e + " field must be an image." },
            in: function(e) { return "The " + e + " field must be a valid value." },
            integer: function(e) { return "The " + e + " field must be an integer." },
            ip: function(e) { return "The " + e + " field must be a valid ip address." },
            length: function(e, n) {
                var t = n[0],
                    i = n[1];
                return i ? "The " + e + " length be between " + t + " and " + i + "." : "The " + e + " length must be " + t + "."
            },
            max: function(e, n) { return "The " + e + " field may not be greater than " + n[0] + " characters." },
            max_value: function(e, n) { return "The " + e + " field must be " + n[0] + " or less." },
            mimes: function(e) { return "The " + e + " field must have a valid file type." },
            min: function(e, n) { return "The " + e + " field must be at least " + n[0] + " characters." },
            min_value: function(e, n) { return "The " + e + " field must be " + n[0] + " or more." },
            not_in: function(e) { return "The " + e + " field must be a valid value." },
            numeric: function(e) { return "The " + e + " field may only contain numeric characters." },
            regex: function(e) { return "The " + e + " field format is invalid." },
            required: function(e) { return "The " + e + " field is required." },
            size: function(e, n) { var t, i, a, r = n[0]; return "The " + e + " size must be less than " + (t = r, i = 1024, a = 0 == (t = Number(t) * i) ? 0 : Math.floor(Math.log(t) / Math.log(i)), 1 * (t / Math.pow(i, a)).toFixed(2) + " " + ["Byte", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"][a]) + "." },
            url: function(e) { return "The " + e + " field is not a valid URL." }
        },
        attributes: {}
    };
    "undefined" != typeof VeeValidate && VeeValidate.Validator.localize(((e = {})[n.name] = n, e));
    return n
});

*/



