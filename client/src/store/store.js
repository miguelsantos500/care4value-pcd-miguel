//Vuex
import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';
import * as Cookies from 'js-cookie';

Vue.use(Vuex);

export const user = {
  id: 0,
  name: '',
  nickname: '',
  email: '',
  admin: 0,
  token: null
};


export const store = new Vuex.Store({
  state: {
    user: user
  },
  plugins: [createPersistedState({//Store user in cookies
    storage: {
      getItem: key => Cookies.get(key),
      setItem: (key, value) => Cookies.set(key, value, {
        expires: 3,
        secure: false
      }),
      removeItem: key => Cookies.remove(key)
    }
  })],
  getters: {
    getUser: state => {
      return state.user
    }
  },
  mutations: {
    saveUser(state, currentUser) {
      state.user = currentUser;
    }
  }
});
Object.assign(user, store.getters.getUser);