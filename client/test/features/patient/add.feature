Feature: Add Patient

  @Add_Patient_1
  Scenario: Add Patient

    Given I go to CARE4VALUE
    Given the Login Page appears
    When I Login with User "admin@mail.com" and Password "123qwerty"
    Then the DashBoard Page appears
    
    Given I go to "PatientManagement"
    Given I click on tab "tabPatientAdd" of page "PatientManagement"
    When the PatientAdd Page appears
    And I set value "CreatePatient1" on "inputName" in page "PatientAdd"
    And I set value "01-01-2000" on "inputBirthDate" in page "PatientAdd"
    And I click on "buttonPatientGenerateCode" in page "PatientAdd"
    And I click on "aButtonSavePatient" in page "PatientAdd"
    Then A success message with text "Patient Created Successfully!" appears

  @Add_Patient_2
  Scenario: Add Patient - Validations

    Given I go to CARE4VALUE
    Given the Login Page appears
    When I Login with User "admin@mail.com" and Password "123qwerty"
    Then the DashBoard Page appears

    Given I go to "PatientManagement"
    Given I click on tab "tabPatientAdd" of page "PatientManagement"
    When the PatientAdd Page appears
    And I click on "aButtonSavePatient" in page "PatientAdd"
    Then Empty Patient Validations appear