Feature: Login

@Logout_1
Scenario: Normal Logout

  Given I go to "Login" url
  Given the Login Page appears
  When I Login with User "other@mail.com" and Password "123qwerty"
  Then the DashBoard Page appears
  When I Logout
  Then the Login Page appears