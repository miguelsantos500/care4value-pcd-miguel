Feature: change user

  @Change_User_1
  Scenario: Change User

    Given I go to CARE4VALUE
    Given the Login Page appears
    When I Login with User "admin@mail.com" and Password "123qwerty"
    Then the DashBoard Page appears
    
    Given I go to "UserManagement"
    When I click on tab "tabUsers" of page "UserManagement"
    When I click on a Show Details button from the list
    Then I fill the inputs fields with name "changeUserTest" and press the Save button
    Then A success message with text "User Updated Successfully!" appears

#user.feature: Change User (No Required Parameters)

  @Change_User_2
  Scenario: Change User

    Given I go to CARE4VALUE
    Given the Login Page appears
    When I Login with User "admin@mail.com" and Password "123qwerty"
    Then the DashBoard Page appears

    Given I go to "UserManagement"
    When I click on tab "tabUsers" of page "UserManagement"
    When I click on a Show Details button from the list
    Then I fill the input field with empty name and a Required field message shloud appear