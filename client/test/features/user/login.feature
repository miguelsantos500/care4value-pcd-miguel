Feature: Login

@Login_1
Scenario: Normal Login

  Given I go to "Login" url
  Given the Login Page appears
  When I Login with User "other@mail.com" and Password "123qwerty"
  Then the DashBoard Page appears

@Login_2
Scenario: Login With No Input

  Given I go to "Login" url
  Given the Login Page appears
  When I attempt to Login with no credentials
  Then Empty Login Validations appear

@Login_3
Scenario: Login With Wrong Credentials

  Given I go to "Login" url
  Given the Login Page appears
  When I Login with User "wrong@mail.com" and Password "wrong"
  Then An error message with text "Incorrect credentials" appears