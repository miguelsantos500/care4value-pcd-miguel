Feature: show users

  @Show_User_1
  Scenario: Get Users

    Given I go to CARE4VALUE
    Given the Login Page appears
    When I Login with User "admin@mail.com" and Password "123qwerty"
    Then the DashBoard Page appears
    
    Given I go to "UserManagement"
    Given I click on tab "tabUsers" of page "UserManagement"
    When the Users Page appears 
    Then a user list appears

  @Show_User_2
  Scenario: Get Users
    When I go to "UserManagement" url
    Then the Login Page appears
    Given I Login with User "other@mail.com" and Password "123qwerty"
    When the DashBoard Page appears
    Then the page "UserManagement" link "is not" present

    When I go to "UserManagement" url 
    Then an alert with text "Permission denied: you have to be administrator to go to this route!" should appear