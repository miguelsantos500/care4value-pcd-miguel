Feature: revoke or grant 

  @Revoke_User_1
  Scenario: Revoke User

    Given I go to CARE4VALUE
    Given the Login Page appears
    When I Login with User "admin@mail.com" and Password "123qwerty"
    Then the DashBoard Page appears
    
    Given I go to "UserManagement"
    When I click on tab "tabUsers" of page "UserManagement"
    When I click on a Block button from the list
    Then A success message with text "User was blocked!" appears

  @Revoke_User_2
  Scenario: Revoke User

    Given I go to CARE4VALUE
    Given the Login Page appears
    When I Login with User "other@mail.com" and Password "123qwerty"
    Then the DashBoard Page appears
    Then the page "UserManagement" link "is not" present


  @Grant_User_1
  Scenario: Grant User

    Given I go to CARE4VALUE
    Given the Login Page appears
    When I Login with User "admin@mail.com" and Password "123qwerty"
    Then the DashBoard Page appears
    
    Given I go to "UserManagement"
    When I click on tab "tabUsers" of page "UserManagement"
    When I click on a Unblock button from the list
    Then A success message with text "User was unblocked!" appears

    @Grant_User_2
      Scenario: Grant User

        Given I go to CARE4VALUE
        Given the Login Page appears
        When I Login with User "other@mail.com" and Password "123qwerty"
        Then the DashBoard Page appears
        Then the page "UserManagement" link "is not" present