@Password_Reset
Feature: Password Reset

  @Password_Reset_1
  Scenario: Password Reset component appears

    Given I go to "Login" url
    Given the Login Page appears
    When I click on I Forgot my password button
    Then the Password Reset component appears with text Password Reset

  @Password_Reset_2
  Scenario: Password Reset No Input (email is required)

    Given I go to "Login" url
    Given the Login Page appears
    When I click on I Forgot my password button
    Then the Password Reset component appears with text Password Reset
    When I click on Reset Password button with no email filled
    Then Empty Email Validations appear

  @Password_Reset_3
  Scenario: Password Reset Wrong Email

    Given I go to "Login" url
    Given the Login Page appears
    When I click on I Forgot my password button
    Then the Password Reset component appears with text Password Reset
    When I fill the Email with "wrong@mail.com"
    Then An error message with text "There is no account with that email address." appears

  @Password_Reset_4
  Scenario: Password Reset Wrong Email

    Given I go to "Login" url
    Given the Login Page appears
    When I click on I Forgot my password button
    Then the Password Reset component appears with text Password Reset
    When I fill the Email with "admin@mail.com"
    Then A success message with text "An Email has been sent to your email with a link for password reset" appears