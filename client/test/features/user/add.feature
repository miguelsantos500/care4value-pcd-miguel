Feature: add user

  @Add_User_1
  Scenario: Add User

    Given I go to CARE4VALUE
    Given the Login Page appears
    When I Login with User "admin@mail.com" and Password "123qwerty"
    Then the DashBoard Page appears
    
    Given I go to "UserManagement"
    Given I click on tab "tabUserAdd" of page "UserManagement"
    When the UserAdd Page appears 
    Then I fill the inputs fields with name "createUserTestName" and password "createUserTestPassword" and password_confirmation "createUserTestPassword" and email "createUserTestMail@mail.com" and press the Save button
    Then A success message with text "User Created Successfully!" appears

  @Add_User_2
  Scenario: Add User

    Given I go to CARE4VALUE
    Given the Login Page appears
    When I Login with User "other@mail.com" and Password "123qwerty"
    Then the DashBoard Page appears
    Then the page "UserManagement" link "is not" present