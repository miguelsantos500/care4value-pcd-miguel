Feature: Change Password

  @Change_Password_1
  Scenario: Change Password

    Given I go to CARE4VALUE
    Given the Login Page appears
    When I Login with User "password@mail.com" and Password "123qwerty"
    Then the DashBoard Page appears
    
    Given I go to "Profile"
    When the Profile Page appears 
    Then I fill the inputs fields with password "123qwerty" and password_confirmation "123qwerty" and press the Save button
    Then A success message with text "Password Changed Successfully!" appears

  @Change_Password_2
  Scenario: Change Password with No Confirmation

    Given I go to CARE4VALUE
    Given the Login Page appears
    When I Login with User "password@mail.com" and Password "123qwerty"
    Then the DashBoard Page appears
    
    Given I go to "Profile"
    When the Profile Page appears 
    Then I fill the inputs fields with password "123qwerty" and password_confirmation "" and press the Save button
    Then Empty Change Password Validations appear