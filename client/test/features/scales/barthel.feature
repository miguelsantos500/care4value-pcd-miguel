@Barthel
Feature: Barthel Testing

  @Barthel_1
  Scenario: Present Information
    Given I go to "Login" url
    Given the Login Page appears
    When I Login with User "admin@mail.com" and Password "123qwerty"
    Then the DashBoard Page appears

    Given I go to "Upload"
    And I click on "radioBtnBarthel" in page "Upload"
    And I set value "P0001" on "inputPatient" in page "Upload"
    When I set file "barthel.xls"
    And I click on "buttonShowDetails" in page "Upload"
    Then I check that "pAuthor" has text "Author: Ana Cordeiro" in page "Upload"


  @Barthel_2
  Scenario: Wrong File
    Given I go to "Login" url
    Given the Login Page appears
    When I Login with User "admin@mail.com" and Password "123qwerty"
    Then the DashBoard Page appears

    Given I go to "Upload"
    And I click on "radioBtnBarthel" in page "Upload"
    And I set value "P0001" on "inputPatient" in page "Upload"
    When I set file "gijon.xls"
    Then An error message with text "Error Parsing File! Please check your chosen file." appears

  @Barthel_3
  Scenario: Already has answers for that date
    Given I go to "Login" url
    Given the Login Page appears
    When I Login with User "admin@mail.com" and Password "123qwerty"
    Then the DashBoard Page appears

    Given I go to "Upload"
    And I click on "radioBtnBarthel" in page "Upload"
    And I set value "P0002" on "inputPatient" in page "Upload"
    When I upload file "barthel_dup.xls"
    Then A success message with text "File Uploaded Successfully!" appears
    Given I go to "Upload"
    And I click on "radioBtnBarthel" in page "Upload"
    And I set value "P0002" on "inputPatient" in page "Upload"
    When I upload file "barthel_dup.xls"
    Then An error message with text "Patient already has answers for that date!" appears
  
  @Barthel_4
  Scenario: Present Information
    Given I go to "Login" url
    Given the Login Page appears
    When I Login with User "admin@mail.com" and Password "123qwerty"
    Then the DashBoard Page appears

    Given I go to "Upload"
    And I click on "radioBtnBarthel" in page "Upload"
    And I set value "P0003" on "inputPatient" in page "Upload"
    When I upload file "barthel_dup.xls"
    Given I go to "Upload"
    And I set value "P0003" on "inputPatient" in page "Upload"
    When I upload file "barthel_dup.xls"
    Then I check that "h3DupAnswers" has text "Answers for this Patient and Date stored in the server:" in page "Upload"




