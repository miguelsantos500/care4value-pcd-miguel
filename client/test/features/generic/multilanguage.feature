Feature: Multilanguage

@Multilanguage_1
Scenario: Check Language

  Given I go to "Login" url
  When the Login Page appears
  Then the Login Page Language is "en"

@Multilanguage_2
Scenario: Change Language

  Given I go to "Login" url
  And the Login Page appears
  When I change the Language to "pt"
  Then the Login Page Language is "pt"