module.exports = {
  url: function() { 
    return process.env.CLIENT_URL + '#/user-management'; 
  },
  elements: {
     linkUserManagement: 'a[id=linkUserManagement]',
     tabUserAdd: 'a[id=tabUserAdd___BV_tab_button__]',
     tabUsers: 'a[id=tabUsers___BV_tab_button__]',
  }
};
