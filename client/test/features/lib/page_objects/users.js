module.exports = {
  url: function() { 
    return process.env.CLIENT_URL + '#/users'; 
  },
  elements: {
    headerUsers: 'h2[id=headerUsers]',
    linkUsers: 'a[id=linkUsers]',
    tableUsers: 'table[id=tableUsers]',
    buttonRevoke: 'button[id="buttonRevokeblockButtonTestUser@mail.com"]',
    buttonGrant: 'button[id="buttonGrantunblockButtonTestUser@mail.com"]',
    buttonDetails: 'button[id="buttonDetailsuserToChange@mail.com"]',
    inputTableName: 'input[id=inputTableName]',
    buttonSaveChanges: 'button[id="buttonSaveChangesuserToChange@mail.com"]',
    spanTableNameErrors: 'span[id=spanTableNameErrors]',
    tdAdminName: { 
      selector: '//td[contains(text(),"admin")]', 
      locateStrategy: 'xpath' 
    }
  }
};
