module.exports = {
  url: function () {
    return process.env.CLIENT_URL;
  },
  elements: {
    /* alertError: 'div[id=alertError]',*/
    alert: 'div[id=alert]',
    alertDanger: '#alert.alert-danger',
    liLogout: 'li[id=liLogout]',
    liLanguageDropDown: 'li[id=liLanguageDropDown]',
    linkLanguageEnglish: 'a[id=linkLanguageEnglish]',
    linkLanguagePortuguese: 'a[id=linkLanguagePortuguese]',
    pErrorModal: 'p[id=pErrorModal]',
    buttonCloseAlert: 'button[class=close]'
  }
};