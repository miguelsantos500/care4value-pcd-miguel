module.exports = {
  url: function() { 
    return process.env.CLIENT_URL + '#/dashboard'; 
  },
  elements: {
  	 headerDashboard: 'h1[id=headerDashBoard]',
  	 linkDashboard: 'a[id=linkDashboard]'
  }
};