module.exports = {
  url: function() { 
    return process.env.CLIENT_URL + '#/profile'; 
  },
  elements: {
     headerProfile: 'h2[id=headerProfile]',
     linkProfile: 'a[id=linkProfile]',
     buttonTogglePassword: 'a[id=buttonTogglePassword]',
     inputNewPassword: 'input[id=inputNewPassword]',
     inputNewPasswordConfirmation: 'input[id=inputNewPasswordConfirmation]',
     buttonSaveNewPassword: 'button[id=buttonSaveNewPassword]',
     spanPasswordConfirmationErrors: 'span[id=spanPasswordConfirmationErrors]',
  }
};