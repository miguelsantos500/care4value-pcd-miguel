module.exports = {
  url: function() { 
    return process.env.CLIENT_URL + '#/user-management'; 
  },
  elements: {
     headerAddUser: 'h2[id=headerAddUser]',
     inputName: 'input[id=inputName]',
     inputPassword: 'input[id=password]',
     inputPasswordConfirmation: 'input[id=password_confirmation]',
     inputEmail: 'input[id=inputEmail]',
     dropDownRole: 'button[id=role__BV_toggle_]',
     buttonResearcher: 'button[id=buttonResearcher]',
     aButtonSaveUser: 'a[id=aButtonSaveUser]'
  }
};