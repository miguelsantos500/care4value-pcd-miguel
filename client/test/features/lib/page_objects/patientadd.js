module.exports = {
  url: () => {
    return process.env.CLIENT_URL + '#/patient-management';
  },
  elements: {
    headerAddPatient: 'h2[id=headerAddPatient]',
    inputName: 'input[id=inputName]',
    inputCode: 'input[id=inputCode]',
    inputBirthDate: 'input[id=inputBirthDate]',
    aButtonSavePatient: 'a[id=aButtonSavePatient]',
    spanInputNameErrors: 'span[id=spanInputNameErrors]',
    spanInputBirthDateErrors: 'span[id=spanInputBirthDateErrors]',
    spanInputCodeErrors: 'span[id=spanInputCodeErrors]',
    buttonPatientGenerateCode: 'button[id=buttonPatientGenerateCode]'
  }
};