module.exports = {
  url: function() { 
    return process.env.CLIENT_URL + '#/login'; 
  },
  elements: {
     headerLogin: 'h2[id=headerLogin]',
     linkLogin: 'a[id=linkLogin]',
     inputLogin: 'input[id=inputLogin]',
  	 inputPassword: 'input[id=inputPassword]',
     inputRecuperationEmail: 'input[id=inputRecuperationEmail]',
  	 buttonLogin: 'button[id=buttonLogin]',
  	 spanLoginErrors: 'span[id=spanLoginErrors]',
  	 spanPasswordErrors: 'span[id=spanPasswordErrors]',
     spanResetEmailPassword: 'span[id=spanResetEmailPassword]',
     buttonIForgotPassword:'button[id=buttonIForgotPassword]',
     h2PasswordResetArea: 'h2[id=h2PasswordResetArea]',
     buttonResetPassword: 'button[id=buttonResetPassword]',
  }
};