module.exports = {
  url: () => {
    return process.env.CLIENT_URL + '#/upload';
  },
  elements: {
    headerUpload: 'h1[id=headerUpload]',
    linkUpload: 'a[id=linkUpload]',
    buttonUpload: 'button[id=buttonUpload]',
    radioBtnBarthel: 'input[value=Barthel]',
    buttonShowDetails: 'button[id=buttonShowDetails]',
    inputFile: 'input[id=inputFile]',
    inputPatient: 'input[id=inputPatient]',
    pAuthor: 'p[id=pAuthor]',
    h3DupAnswers: 'h3[id=h3DupAnswers]'
  }
};