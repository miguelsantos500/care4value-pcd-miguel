module.exports = {
  url: () => {
    return process.env.CLIENT_URL + '#/patient-management';
  },
  elements: {
     linkPatientManagement: 'a[id=linkPatientManagement]',
     tabPatientAdd: 'a[id=tabPatientAdd___BV_tab_button__]',
     tabPatients: 'a[id=tabPatients___BV_tab_button__]',
  }
};
