const {client} = require('nightwatch-cucumber');

const chai = require('chai')
  , assert = chai.assert;

const DEFAULT_WAIT_TIME = 3000;

const routes = {
  HomePage: "/"
};


const {AfterAll, BeforeAll} = require('cucumber');
const axios = require('axios');
const allConfigs = require(process.cwd() + '/../config/custom-config');
let customConfigs = {};
if (allConfigs.env) {
  customConfigs = allConfigs[allConfigs.env];
} else {
  console.warn('No Configuration Found!');
}
const serverUrl = customConfigs.SERVER_URL || "http://localhost:7555";
BeforeAll(() => {
  return axios.post(serverUrl + '/testing',
    {testing: true, password: 'finalProject2018'});
});

AfterAll(() => {
  return axios.post(serverUrl + '/testing',
    {testing: false, password: 'finalProject2018'})
});


//Aux functions for testing

getPO = function (poName) {
  poName = poName.toLowerCase();

  switch (poName) {
    case 'app':
      return client.page.app();
    case 'login':
      return client.page.login();
    case 'dashboard':
      return client.page.dashboard();
    case 'users':
      return client.page.users();
    case 'useradd':
      return client.page.useradd();
    case 'usermanagement':
      return client.page.usermanagement();
    case 'patientadd':
      return client.page.patientadd();
    case 'patientmanagement':
      return client.page.patientmanagement();
    case 'profile':
      return client.page.profile();
    case 'upload':
      return client.page.upload();
    default:
      throw "Page Object with name '" + poName + "' does not exist!";
  }
};


module.exports = {
  DEFAULT_WAIT_TIME: DEFAULT_WAIT_TIME
};