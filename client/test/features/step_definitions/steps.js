const {nightwatch, client} = require('nightwatch-cucumber');
const {Before, After, Given, Then, When} = require('cucumber');

const chai = require('chai')
  , assert = chai.assert
  , expect = chai.expect
  , should = chai.should();

const {DEFAULT_WAIT_TIME} = require('../lib/support');
const {saveScreenshot} = require('../../screenshots');

/*
Before((scenario) => new Promise(resolve => {
  console.log('Before start');
  setTimeout(() => {
    console.log('Before end');
    resolve();
  }, 1000);
}));*/

After((scenario) => new Promise(resolve => {
  //Think of only clearing cache/localStorage/sessionStorage
  return client.log('Resetting browser...')
    .pause(500)
    .deleteCookies()
    .execute(function () {
      window.localStorage.clear();
      return true;
    }).refresh().then(() => {
      resolve();
    });
}));

Given(/^I go to CARE4VALUE$/, () => {

  return client.url(process.env.CLIENT_URL)
    .assert.title('CARE4VALUE');
});

Given(/^I go to "([^"]*)"$/, (route) => {
  const pageObject = getPO(route);
  return pageObject.click('@link' + route)
    .assert.title('CARE4VALUE');
});

Given(/^I go to "([^"]*)" url$/, (route) => {
  const pageObject = getPO(route);

  //The following runs what is inside the execute() on the browser
  //There is no other way since our page redirects, which makes
  //nightwatch not recognize the navigation.
  return client
    .execute(function (url) {
      this.document.location = url//'http://localhost:8080/#/login';
    }, [pageObject.url()]);

});


Then(/^An error message with text "([^"]*)" appears$/, (errorText) => {
  const app = getPO('App'); //Page Object
  return app
    .log('Checking if Error Alert with text ' + errorText + ' is present...')
    .waitForElementVisible('@alertDanger', DEFAULT_WAIT_TIME)
    .assert.containsText('@alertDanger', errorText);
  /* //Using Chai, with getText, and manually saving screenshot
  .getText('@alertError', function(result) {
    if(result.value !== errorText) {//to save screenshot
        saveScreenshot(client);
      }
    result.value.should.have.string(errorText);

    return app;
  });*/
});


Then(/^the Login Page appears$/, () => {
  const login = getPO('Login'); //Page Object
  return login
    .waitForElementVisible('@inputLogin', DEFAULT_WAIT_TIME)
    .waitForElementVisible('@inputPassword', DEFAULT_WAIT_TIME)
    .assert.visible('@inputPassword')
    .assert.visible('@buttonLogin');
});

Then(/^the UserAdd Page appears$/, () => {
  const login = getPO('UserAdd');
  return login
    .waitForElementVisible('@headerAddUser', DEFAULT_WAIT_TIME)
    .assert.visible('@inputName')
    .assert.visible('@inputPassword')
    .assert.visible('@inputPasswordConfirmation')
    .assert.visible('@inputEmail')
    .assert.visible('@dropDownRole');
});

Given(/^the PatientAdd Page appears$/, () => {
  const pageObject = getPO('PatientAdd');

  return pageObject
    .waitForElementVisible('@headerAddPatient', DEFAULT_WAIT_TIME);
});

Then(/^the DashBoard Page appears$/, () => {
  const dashboard = getPO('DashBoard');
  return dashboard
    .waitForElementVisible('@headerDashboard', DEFAULT_WAIT_TIME);
});

Then(/^the Users Page appears$/, () => {
  const users = getPO('Users');
  return users
    .waitForElementVisible('@headerUsers', DEFAULT_WAIT_TIME)
    .assert.containsText('@headerUsers', 'Users');
});

Then(/^the Profile Page appears$/, () => {
  const login = getPO('Profile'); //Page Object
  return login
    .waitForElementVisible('@headerProfile', DEFAULT_WAIT_TIME);
});


////    Login   ////


Given(/^I Login with User "([^"]*)" and Password "([^"]*)"$/, (email, password) => {
  const login = getPO('Login'); //Page Object
  //It's way easier this way than having to do an overly complicated method
  //because of "if"+"async"
  if (!email || !password) {
    throw 'Empty Email or Password! Please use "I attempt to Login with no credentials"' +
    ' if you want to test that!';
  }
  return login
    .log('Logging in with ' + email + '/' + password + '...')
    .setValue('@inputLogin', email)
    .setValue('@inputPassword', password)
    .click('@buttonLogin');
});

Given(/^I attempt to Login with no credentials$/, () => {
  const login = getPO('Login'); //Page Object

  return login
    .log('Checking if Login form has no values...')

    .getValue('@inputLogin', function (result) {//WARNING => Can't use .then callbacks => .then((result) => {
      console.log(result.value)
      if (result.value) throw 'inputLogin has Setted Value » ' + result.value;
      return;
    })
    .then(() => {

      return login
        .getText('@inputPassword', function (result) {//.then((value) => {
          if (result.value) throw 'inputPassword has Setted Value » ' + result.value;
          return;
        })
        .then(() => {

          return login
            .click('@buttonLogin');
        });
    });
});


Then(/^Empty Login Validations appear$/, () => {
  const login = getPO('Login');

  return login
    .assert.containsText("@spanLoginErrors", 'Required field.')
    .assert.containsText("@spanPasswordErrors", 'Required field.');

});

////    Logout   ////

When(/^I Logout$/, () => {
  const app = getPO('App');

  return app
    .click('@liLogout');
});


////    Multilanguage   ////


Then(/^the Login Page Language is "([^"]*)"$/, (language) => {
  const login = getPO('Login');

  const headerLoginText = require('../../../src/assets/multilanguage')
    .multilanguageJSON[language]
    .loginArea;

  return login
    .assert.containsText("@headerLogin", headerLoginText);
});

When(/^I change the Language to "([^"]*)"$/, (language) => {
  const app = getPO('App');

  let linkLanguage = '';
  switch (language) {
    case 'PT', 'pt':
      linkLanguage = '@linkLanguagePortuguese';
      break;
    case 'EN', 'en':
      linkLanguage = '@linkLanguageEnglish';
      break;
    default:
      throw "Language with name '" + language + "' does not exist!";
  }

  return app
    .click('@liLanguageDropDown')
    .click(linkLanguage);
});


////    Users   ////

Then(/^a user list appears$/, () => {
  const users = getPO('Users'); //Page Object
  return users
    .waitForElementVisible('@tableUsers', DEFAULT_WAIT_TIME)
    .assert.visible('@tdAdminName');
});

//the page "Users" link "is not" present
Then(/^the page "([^"]*)" link "(is|is not)" present$/, (route, isPresent) => {
  const pageObject = getPO(route);

  //if this function receives "is" then the element linkUsers is expected to be present
  if (isPresent == 'is') {
    return pageObject
      .expect.element('@link' + route).to.be.present;
  }  //if this function receives "is not" then the element linkUsers is expected to not be present
  return pageObject
    .expect.element('@link' + route).to.not.be.present;

});

Then(/^an alert with text "([^"]*)" should appear$/, (errorMessage) => {
  const pageObject = getPO('App'); //page object is from app.vue

  return pageObject
    .waitForElementVisible('@pErrorModal', DEFAULT_WAIT_TIME)
    .assert.containsText('@pErrorModal', errorMessage);

});


Given(/^I click on tab "([^"]*)" of page "([^"]*)"$/, (tab, route) => {
  const pageObject = getPO(route);


  return pageObject
    .waitForElementVisible('@' + tab, DEFAULT_WAIT_TIME)
    .click('@' + tab);


});

Then(/^I fill the inputs fields with name "([^"]*)" and password "([^"]*)" and password_confirmation "([^"]*)" and email "([^"]*)" and press the Save button$/, (name, password, password_confirmation, email) => {
  const pageObject = getPO('UserAdd');

  console.log(name);
  console.log(password);
  console.log(password_confirmation);
  console.log(email);
  return pageObject
    .log('Attempting to create user...')
    .setValue('@inputName', name)
    .setValue('@inputPassword', password)
    .setValue('@inputPasswordConfirmation', password_confirmation)
    .setValue('@inputEmail', email)
    .click('@dropDownRole')
    .click('@buttonResearcher')
    .click('@aButtonSaveUser');
});

Then(/^I fill the inputs fields with password "([^"]*)" and password_confirmation "([^"]*)" and press the Save button$/, (password, password_confirmation) => {
  const pageObject = getPO('Profile');

  return pageObject
    .log('Attempting to change Password...')
    .click('@buttonTogglePassword')
    .waitForElementVisible('@inputNewPassword', DEFAULT_WAIT_TIME)
    .setValue('@inputNewPassword', password)
    .setValue('@inputNewPasswordConfirmation', password_confirmation)
    .click('@buttonSaveNewPassword');
});

Then(/^Empty Change Password Validations appear$/, () => {
  const profile = getPO('Profile');

  return profile
    .assert.containsText("@spanPasswordConfirmationErrors", 'Required field.');

});

Then(/^A success message with text "([^"]*)" appears$/, (text) => {
  const app = getPO('App');

  return app
    .log('Checking if Alert with text ' + text + ' is present...')
    .waitForElementVisible('@alert', DEFAULT_WAIT_TIME)
    .assert.containsText("@alert", text);
});

Given(/^I click on a Show Details button from the list$/, () => {
  const pageObject = getPO("Users");

  return pageObject
    .waitForElementVisible('@tableUsers', DEFAULT_WAIT_TIME)
    .click('@buttonDetails');
});

Then(/^I fill the inputs fields with name "([^"]*)" and press the Save button$/, (name) => {
  const pageObject = getPO('Users');

  console.log(name);

  return pageObject
    .log('Attempting to update user...')
    .setValue('@inputTableName', name)
    .click('@buttonSaveChanges');
});

Then(/^I fill the input field with empty name and a Required field message shloud appear$/, () => {
  const pageObject = getPO('Users');

  return pageObject
    .log('Attempting to update user...')
    .clearValue('@inputTableName')
    .waitForElementVisible('@spanTableNameErrors', 3000)
    .assert.containsText('@spanTableNameErrors', 'Required field.');
});

Given(/^I click on a Block button from the list$/, () => {
  const pageObject = getPO("Users");

  return pageObject
    .waitForElementVisible('@tableUsers', DEFAULT_WAIT_TIME)
    .click('@buttonRevoke');

});

Given(/^I click on a Unblock button from the list$/, () => {
  const pageObject = getPO("Users");

  return pageObject
    .waitForElementVisible('@tableUsers', DEFAULT_WAIT_TIME)
    .click('@buttonGrant');
});

//////////////////////////////////////////////////Password reset

When(/^I click on I Forgot my password button$/, () => {
  const pageObject = getPO("Login");

  return pageObject
    .waitForElementVisible('@buttonIForgotPassword', DEFAULT_WAIT_TIME)
    .click('@buttonIForgotPassword');
});

Then(/^the Password Reset component appears with text Password Reset$/, () => {
  const pageObject = getPO('Login');
  return pageObject
    .waitForElementVisible('@h2PasswordResetArea', DEFAULT_WAIT_TIME)
    .assert.containsText('@h2PasswordResetArea', 'Password Reset');
});

When(/^I click on Reset Password button with no email filled$/, () => {
  const pageObject = getPO("Login");

  return pageObject
    .waitForElementVisible('@buttonResetPassword', DEFAULT_WAIT_TIME)
    .click('@buttonResetPassword');
});

//Empty Email Validations appear
Then(/^Empty Email Validations appear$/, () => {
  const pageObject = getPO('Login');

  return pageObject
    .waitForElementVisible('@spanResetEmailPassword', DEFAULT_WAIT_TIME)
    .assert.containsText('@spanResetEmailPassword', 'Required field.');
});

Then(/^Empty Patient Validations appear$/, () => {
  const pageObject = getPO('PatientAdd');

  return pageObject
    .waitForElementVisible('@spanInputNameErrors', DEFAULT_WAIT_TIME)
    .assert.containsText('@spanInputNameErrors', 'Required field.')
    .assert.containsText('@spanInputCodeErrors', 'Required field.');
});

Given(/^I fill the Email with "([^"]*)"$/, (email) => {
  const pageObject = getPO('Login');

  return pageObject
    .setValue('@inputRecuperationEmail', email)
    .click('@buttonResetPassword');
});

Given(/^I click on "([^"]*)" in page "([^"]*)"$/, (element, page) => {
  const pageObject = getPO(page);

  return pageObject
    .waitForElementVisible('@' + element, DEFAULT_WAIT_TIME)
    .click('@' + element);
});


Given(/^I set file "([^"]*)"$/, (file) => {
  const pageObject = getPO('Upload');

  return pageObject
    .uploadLocalFile(require('path').resolve(__dirname + '../../../data/' + file), 'input[id=inputFile]')/*
    .setValue('@inputFile', require('path').resolve(__dirname + '../../../data/' + file))*/;
});
Given(/^I upload file "([^"]*)"$/, (file) => {
  const pageObject = getPO('Upload');

  return pageObject
    .uploadLocalFile(require('path').resolve(__dirname + '../../../data/' + file), 'input[id=inputFile]')/*
    .setValue('@inputFile', require('path').resolve(__dirname + '../../../data/' + file))*/
    .click('@buttonUpload');
});
Given(/^I check that "([^"]*)" has text "([^"]*)" in page "([^"]*)"$/, (element, text, page) => {
  const pageObject = getPO(page);

  return pageObject
    .waitForElementVisible('@' + element, DEFAULT_WAIT_TIME)
    .assert.containsText('@' + element, text);
});
Given(/^I set value "([^"]*)" on "([^"]*)" in page "([^"]*)"$/, (text, element, page) => {
  const pageObject = getPO(page);

  return pageObject
    .waitForElementVisible('@' + element, DEFAULT_WAIT_TIME)
    .clearValue('@' + element)
    .setValue('@' + element, text);
});

Given(/^I clear value on "([^"]*)" in page "([^"]*)"$/, (element, page) => {
  const pageObject = getPO(page);

  return pageObject
    .waitForElementVisible('@' + element, DEFAULT_WAIT_TIME)
    .click('@' + element)
    .clearValue('@' + element);
});
