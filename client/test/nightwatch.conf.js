require('env2')('.env');
const seleniumServer = require('selenium-server');
const chromedriver = require('chromedriver');


const {imgpath, SCREENSHOT_PATH} = require('./screenshots');


require('nightwatch-cucumber')({
    cucumberArgs: ['--require', 'test/features/step_definitions',
        '--format', 'json:test/reports/cucumber.json',
        'test/features']
});
module.exports = {
    page_objects_path: 'test/features/lib/page_objects',
    custom_assertions_path: '',
    custom_commands_path: 'test/features/lib/nightwatch_custom_commands',
    live_output: false,
    disable_colors: false,
    selenium: {
        start_process: true,
        server_path: seleniumServer.path,
        host: '127.0.0.1',
        port: 4444,
        cli_args: {
            'webdriver.chrome.driver': chromedriver.path
        }
    },
    test_settings: {
        default: {
            screenshots: {
                on_failure: true,
                enabled: true, // if you want to keep screenshots
                path: SCREENSHOT_PATH // save screenshots here
            },
            globals: {
                waitForConditionTimeout: 5000 // sometimes internet is slow so wait.
            },
            desiredCapabilities: { // use Chrome as the default browser for tests
                browserName: 'chrome'
            }
        },
        firefox: {
            desiredCapabilities: {
                browserName: 'firefox',
                javascriptEnabled: true,
                acceptSslCerts: true
            }
        }
    }
};


module.exports.imgpath = imgpath;
module.exports.SCREENSHOT_PATH = SCREENSHOT_PATH;