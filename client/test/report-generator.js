const reporter = require('cucumber-html-reporter');

const options = {
        name: "CARE4VALUE",
        brandTitle: "CARE4VALUE Frontend Tests Report",
        theme: 'hierarchy', //Available: ['bootstrap', 'hierarchy', 'foundation', 'simple'] Type: String
        jsonFile: 'test/reports/cucumber.json',
        output: 'test/reports/cucumber_report.html',
        reportSuiteAsScenarios: true,
        launchReport: true,
        /*metadata: {
            "App Version":"0.3.2",
            "Test Environment": "STAGING",
            "Browser": "Chrome  54.0.2840.98",
            "Platform": "Windows 10",
            "Parallel": "Scenarios",
            "Executed": "Remote"
        }*/
    };
 
console.log('Generating HTML Report....');

reporter.generate(options);