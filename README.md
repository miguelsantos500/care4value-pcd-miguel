# Care4Value PCD

This is a final project for IPL-ESTG Computer Science degree.

The goal of this project is to develop an application that allows the automatic import of a data source in different Excels formats, in order to store, visualize and analyze at a later date.

Made with:

- Vuejs
- Nodejs
- Mysql
